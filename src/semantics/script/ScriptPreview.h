/*
MIT License

Copyright (c) 2022 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo/loom
*/

#ifndef simodo_sbl_ScriptPreview
#define simodo_sbl_ScriptPreview

/*! \file ScriptPreview.h
    \brief Интерпретация операторов SBL внутренней машиной SIMODO
*/

#include "ScriptSemantics_abstract.h"

namespace simodo::interpret
{
    class ScriptPreview: public ScriptSemantics_abstract
    {
    public:
        ScriptPreview(ModuleManagement_interface & module_management);

    public:
        virtual bool    checkInterpretType(InterpretType interpret_type) const override;
    };

}

#endif // simodo_sbl_ScriptPreview

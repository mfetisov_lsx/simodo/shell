/*
MIT License

Copyright (c) 2022 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo/loom
*/

#include "SystemVerilogSemantics_abstract.h"
#include "simodo/interpret/AnalyzeException.h"
#include "simodo/bormental/DrBormental.h"
#include "simodo/inout/convert/functions.h"
#include "simodo/inout/format/fmt.h"

#include "simodo/interpret/StackOfNames.h"

#include <cassert>

using namespace simodo;

namespace sv
{
    SystemVerilogSemantics_abstract::SystemVerilogSemantics_abstract(interpret::ModuleManagement_interface & module_management)
        : _module_management(module_management)
    {
    }

    SystemVerilogSemantics_abstract::~SystemVerilogSemantics_abstract()
    {
    }

    void SystemVerilogSemantics_abstract::reset()
    {
    }

    interpret::InterpretState SystemVerilogSemantics_abstract::before_start() 
    { 
        return interpret::InterpretState::Flow; 
    }

    interpret::InterpretState SystemVerilogSemantics_abstract::before_finish(interpret::InterpretState state) 
    { 
        return state; 
    }

    bool SystemVerilogSemantics_abstract::isOperationExists(ast::OperationCode operation_code) const
    {
        SystemVerilogOperationCode operation = static_cast<SystemVerilogOperationCode>(operation_code);

        switch(operation)
        {
        case SystemVerilogOperationCode::None:
        case SystemVerilogOperationCode::PushArrayConst:
        case SystemVerilogOperationCode::Assign:
            return true;
        default:
            return false;
        }
    }

    interpret::InterpretState SystemVerilogSemantics_abstract::performOperation(const ast::Node & op) 
    {
        SystemVerilogOperationCode operation = static_cast<SystemVerilogOperationCode>(op.operation());

        switch(operation)
        {
        case SystemVerilogOperationCode::None:
            return executeNone();
        case SystemVerilogOperationCode::PushArrayConst:
            return executePushArrayConst(op);
        case SystemVerilogOperationCode::Assign:
            return executeAssign(op);
        default:
            break;
        }

        throw bormental::DrBormental("SystemVerilogSemantics_abstract::performOperation", 
                            inout::fmt("Invalid operation index %1")
                                .arg(op.operation()));
    }

    interpret::InterpretState SystemVerilogSemantics_abstract::executeNone()
    {
        return interpret::InterpretState::Flow;
    }

    interpret::InterpretState SystemVerilogSemantics_abstract::executePushArrayConst(const simodo::ast::Node & op)
    {
        assert(_interpret);

        variable::Variable   constant_variable;
        [[maybe_unused]] size_t index = 0;

        switch(op.token().type())
        {
        case inout::LexemeType::Annotation:
            constant_variable = { u"", op.token().lexeme(), op.token().location() };
            break;
        case inout::LexemeType::Number:
            if (op.token().lexeme().substr(0,2) == u"'b") {
                std::string number_string = inout::toU8(op.token().lexeme().substr(2));
                constant_variable = { u"", 
                        static_cast<int64_t>(std::stoll(number_string,nullptr,2)), 
                        op.token().location() };
            }
            else if (op.token().lexeme().substr(0,2) == u"'o") 
                constant_variable = { u"", 
                        static_cast<int64_t>(std::stoll(inout::toU8(op.token().lexeme().substr(2)),nullptr,8)), 
                        op.token().location() };
            else if (op.token().lexeme().substr(0,2) == u"'h") 
                constant_variable = { u"", 
                        static_cast<int64_t>(std::stoll(inout::toU8(op.token().lexeme().substr(2)),nullptr,16)), 
                        op.token().location() };
            else if (op.token().qualification() == inout::TokenQualification::Integer)
                constant_variable = { u"", static_cast<int64_t>(std::stoll(inout::toU8(op.token().lexeme()))), op.token().location() };
            else
                constant_variable = { u"", std::stod(inout::toU8(op.token().lexeme())), op.token().location() };
            break;
        case inout::LexemeType::Punctuation:
            if (op.token().lexeme() == u"true" || op.token().lexeme() == u"false")
                constant_variable = { u"", op.token().lexeme() == u"true", op.token().location() };
            else if (op.token().lexeme() == u"null") {
                constant_variable = variable::null_variable(op.token().location());
                index = constant_variable.value().variant().index();
            }
            break;
        default:
            throw bormental::DrBormental("SystemVerilogSemantics_abstract::executePushConstant", 
                                        inout::fmt("Invalid internal type of constant to convert (%1)")
                                            .arg(getLexemeTypeName(op.token().type())));
        }

        if (!op.branches().empty()) {
            std::vector<variable::Value> unit_parts;

            for(const ast::Node & n : op.branches())
                unit_parts.push_back(n.token().lexeme());

            constant_variable.spec().set(u"unit", unit_parts);
        }

        stack().push(constant_variable);

        return interpret::InterpretState::Flow;
    }

    interpret::InterpretState SystemVerilogSemantics_abstract::executeAssign(const simodo::ast::Node & )
    {
        assert(_interpret);

        interpret::name_index_t left  = stack().top(1);
        interpret::name_index_t right = stack().top(0);

        variable::Variable & left_var        = stack().variable(left).origin();
        const variable::Variable & right_var = stack().variable(right).origin();

        if (left_var.value().isArray() && right_var.type() == variable::ValueType::Int) {
            std::shared_ptr<variable::Array> array_variable = left_var.value().getArray();
            if (array_variable->dimensions().size() != 1)
                throw bormental::DrBormental("SystemVerilogSemantics_abstract::executeAssign", 
                                            inout::fmt("Invalid internal type..."));
            variable::index_t            size = array_variable->dimensions()[0];
            std::vector<variable::Value> array;
            int64_t                      var = right_var.value().getInt();

            for(variable::index_t i=0; i < size; ++i ) {
                array.push_back(var & 1);
                var >>= 1;
            }
            left_var.value() = array;
        }
        else
            left_var.value() = right_var.value().copy();

        stack().pop(2);

        return interpret::InterpretState::Flow;
    }


}
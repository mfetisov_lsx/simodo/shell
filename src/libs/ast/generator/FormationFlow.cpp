/*
MIT License

Copyright (c) 2021 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo/loom
*/

#include "simodo/ast/generator/FormationFlow.h"

#include <cassert>

namespace simodo::ast
{
    FormationFlow::FormationFlow() 
    {
        _flow.push_back(&_tree.mutable_root());
        _flow.back()->_branches.reserve(8);
    }

    void FormationFlow::addNode(const std::u16string & host, 
                                OperationCode op, 
                                const inout::Token & op_symbol, 
                                const inout::Token & bound)
    {
        assert(!_flow.empty());

        // Создаём ноду, остаёмся в старой
        _flow.back()->_branches.emplace_back(host, op, op_symbol, bound);
    }

    void FormationFlow::addNode_StepInto(const std::u16string & host, 
                                OperationCode op, 
                                const inout::Token & op_symbol, 
                                const inout::Token & bound)
    {
        assert(!_flow.empty());

        // Создаём ноду и переключаемся на неё
        _flow.back()->_branches.emplace_back(host, op, op_symbol, bound);
        _flow.push_back( &(_flow.back()->_branches.back()) );
    }

    bool FormationFlow::goParent()
    {
        assert(!_flow.empty());

        if (_flow.size() < 2)
            return false;

        // Возвращаемся на вышестоящую ноду
        _flow.pop_back();
        return true;
    }

    void FormationFlow::addFile(const std::string & file_path)
    {
        _tree._files.push_back(file_path);
    }

}
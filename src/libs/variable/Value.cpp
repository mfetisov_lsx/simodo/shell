/*
MIT License

Copyright (c) 2022 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo/loom
*/

#include "simodo/variable/Variable.h"

#include <cassert>

namespace simodo::variable
{
    const VariableRef & Value::getRef() const 
    { 
        return std::get<VariableRef>(_variant); 
    }

    Value Value::copy() const
    {
        switch(_type)
        {
        case ValueType::Array:
            if (std::holds_alternative<std::shared_ptr<Array>>(_variant))
                return std::make_shared<Array>(getArray()->copy());
            return _type;
        case ValueType::Function:
            /// \todo При копировании функции ломается передача замыканий (нужно проверить)
            // return *this;
        case ValueType::Object:
            if (std::holds_alternative<std::shared_ptr<Object>>(_variant))
                return { _type, std::make_shared<Object>(getObject()->copy()) };
            return _type;
        case ValueType::Ref:
            assert(getRef().origin().type() != ValueType::Ref);
            return getRef().origin().value().copy();
        default:
            return *this;
        }
    }

}
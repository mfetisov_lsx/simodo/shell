/*
MIT License

Copyright (c) 2019 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo/loom
*/

#include "simodo/inout/token/RegularInputStreamSupplier.h"
#include "simodo/inout/token/FileStream.h"

namespace simodo::inout
{
    std::shared_ptr<InputStream_interface> RegularInputStreamSupplier::supply(const std::string & path)
    {
        std::shared_ptr<InputStream_interface> in = std::make_shared<FileStream>(path);

        if (in->good())
            return in;

        return std::shared_ptr<InputStream_interface>();
    }

}
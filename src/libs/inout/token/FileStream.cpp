/*
MIT License

Copyright (c) 2021 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo
*/

#include "simodo/inout/token/FileStream.h"

#include "simodo/inout/convert/functions.h"

#include <cassert>
#include <cstdint>

namespace simodo::inout
{

    FileStream::FileStream(const std::string & path)
        : InputStream(_in)
    {
        _in.open(SIMODO_INOUT_STD_STRING(path));
    }

}
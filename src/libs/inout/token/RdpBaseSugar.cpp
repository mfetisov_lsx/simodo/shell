/*
MIT License

Copyright (c) 2021 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo
*/

#include "simodo/inout/token/RdpBaseSugar.h"
#include "simodo/inout/format/fmt.h"

namespace simodo::inout
{
    bool RdpBaseSugar::reportUnexpected(const Token & t, const std::string & expected) const
    {
        _m.reportError(t.makeLocation(_files),
                    // u"Irrelevant symbol '" + t.lexeme() + u"'" + (expected.empty() ? u"" : u", expected: " + expected));
                    fmt("Irrelevant symbol '%1'%2")
                    .arg(t.lexeme())
                    .arg(expected.empty() ? "" : ", expected: %3")
                    .arg(expected));

        return false;
    }

}
/*
MIT License

Copyright (c) 2021 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo
*/

#include "simodo/inout/token/Lexeme.h"
#include "simodo/inout/convert/functions.h"

namespace simodo::inout
{

std::string getLexemeMnemonic(const Lexeme & lex)
{
    std::string s;

    switch(lex.type())
    {
    case LexemeType::Id:
        s = "(идентификатор)";
        break;
    case LexemeType::Empty:
        s = "(конец файла)";
        break;
    case LexemeType::Error:
        s = "(ошибка лексики)";
        break;
    case LexemeType::Number:
        s = "(число)";
        break;
    case LexemeType::Comment:
        s = "(комментарий)";
        break;
    case LexemeType::Compound:
        s = inout::toU8(lex.lexeme());
        break;
    case LexemeType::Annotation:
        s = "(строковая константа)";
        break;
    case LexemeType::Punctuation:
        s = "'" + inout::toU8(lex.lexeme()) + "'";
        break;
    default: // default: нужен на случай расширения перечисления, чтобы видеть ошибку
        s = "(***)";
        break;
    }

    return s;
}

}
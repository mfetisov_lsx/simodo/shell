/*
MIT License

Copyright (c) 2019 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo
*/

#include "simodo/parser/fuze/BaseOperationCode.h"

namespace simodo::parser
{
    const char * getBaseOperationCodeName(BaseOperationCode op) noexcept
    {
        switch(op)
        {
        case BaseOperationCode::None:
            return "None";
        case BaseOperationCode::PushConstant:
            return "PushConstant";
        case BaseOperationCode::PushVariable:
            return "PushVariable";
        case BaseOperationCode::ObjectElement:
            return "ObjectElement";
        case BaseOperationCode::FunctionCall:
            return "FunctionCall";
        case BaseOperationCode::ProcedureCheck:
            return "ProcedureCheck";
        case BaseOperationCode::Print:
            return "Print";
        case BaseOperationCode::Block:
            return "Block";
        case BaseOperationCode::Pop:
            return "Pop";
        default:
            return "***";
        }
    }

}
#include "Initialized.h"
#include "simodo/lsp-server/ServerContext.h"

namespace simodo::lsp
{

void Initialized::work()
{
    _server.initialized();
}

}
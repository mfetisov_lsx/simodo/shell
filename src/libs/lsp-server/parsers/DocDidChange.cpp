#include "DocDidChange.h"
#include "simodo/lsp-server/ServerContext.h"

#include "simodo/variable/json/Rpc.h"
#include "simodo/variable/json/Serialization.h"

namespace simodo::lsp
{

void DocDidChange::work()
{
    if (_jsonrpc.is_valid() && _jsonrpc.params().type() == variable::ValueType::Object) {
        if (_server.changeDocument(*_jsonrpc.params().getObject()))
            return;
    }
    _server.log().error("There are wrong parameter structure of 'textDocument/didChange' notification", variable::toJson(_jsonrpc.value()));
    _server.sending().push(
        /// @todo Скорректировать коды (и тексты) ошибок
        variable::JsonRpc(-1,u"There are wrong parameter structure of 'textDocument/didChange' notification", -1));
}

}
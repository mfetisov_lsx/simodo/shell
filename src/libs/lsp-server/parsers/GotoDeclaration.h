#ifndef GotoDeclaration_h
#define GotoDeclaration_h

#include "simodo/tp/Task_interface.h"
#include "simodo/variable/json/Rpc.h"
#include "simodo/variable/Variable.h"

namespace simodo::lsp
{

class ServerContext;

class GotoDeclaration : public tp::Task_interface
{
    ServerContext &     _server;
    variable::JsonRpc _jsonrpc;

public:
    GotoDeclaration() = delete;
    GotoDeclaration(ServerContext & server, std::string jsonrpc)
        : _server(server), _jsonrpc(jsonrpc)
    {}

    virtual void work() override;
};

}

#endif // GotoDeclaration_h
#ifndef TaskSemanticTokens_h
#define TaskSemanticTokens_h

#include "simodo/tp/Task_interface.h"
#include "simodo/variable/json/Rpc.h"
#include "simodo/variable/Variable.h"

namespace simodo::lsp
{

class ServerContext;

class SemanticTokens : public tp::Task_interface
{
    ServerContext &     _server;
    variable::JsonRpc _jsonrpc;

public:
    SemanticTokens() = delete;
    SemanticTokens(ServerContext & server, std::string jsonrpc)
        : _server(server), _jsonrpc(jsonrpc)
    {}

    virtual void work() override;

private:
};

}

#endif // TaskSemanticTokens_h
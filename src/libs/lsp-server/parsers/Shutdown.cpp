#include "Shutdown.h"
#include "simodo/lsp-server/ServerContext.h"

#include "simodo/variable/json/Rpc.h"
#include "simodo/variable/json/Serialization.h"

namespace simodo::lsp
{

void Shutdown::work()
{
    if (_server.state() == ServerState::Shutdown) {
        _server.log().error("There are wrong parameter structure of 'shutdown' command", variable::toJson(_jsonrpc.value()));
        /// @todo Скорректировать коды (и тексты) ошибок
        _server.sending().push(variable::JsonRpc(-1, u"Invalid request", _jsonrpc.id()));
        return;
    }

    if (_server.shutdown()) {
        _server.sending().push(variable::JsonRpc(variable::Value {}, _jsonrpc.id()));
        return;
    }

    _server.log().error("There are wrong parameter structure of 'shutdown' command", variable::toJson(_jsonrpc.value()));
    _server.sending().push(
        /// @todo Скорректировать коды (и тексты) ошибок
        variable::JsonRpc(-1, u"An exception happens during shutdown request", _jsonrpc.id()));
}

}
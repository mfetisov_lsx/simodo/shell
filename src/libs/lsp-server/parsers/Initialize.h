#ifndef TaskInitialize_h
#define TaskInitialize_h

#include "simodo/lsp-client/ClientParams.h"
#include "simodo/tp/Task_interface.h"
#include "simodo/variable/json/Rpc.h"
#include "simodo/variable/Variable.h"

namespace simodo::lsp
{

class ServerContext;

class Initialize : public tp::Task_interface
{
    ServerContext &     _server;
    variable::JsonRpc _jsonrpc;

    lsp::ClientParams   _params;

public:
    Initialize() = delete;
    Initialize(ServerContext & server, std::string jsonrpc)
        : _server(server), _jsonrpc(jsonrpc)
    {}

    virtual void work() override;
};

}

#endif // TaskInitialize_h
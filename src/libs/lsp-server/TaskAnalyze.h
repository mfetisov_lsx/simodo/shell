#ifndef TaskAnalyze_h
#define TaskAnalyze_h

#include "simodo/tp/Task_interface.h"

namespace simodo::lsp
{

class DocumentContext;

class TaskAnalyze : public simodo::tp::Task_interface
{
    DocumentContext & _doc;

public:
    TaskAnalyze() = delete;
    TaskAnalyze(DocumentContext & doc);

    virtual void work() override;
};

}

#endif // TaskAnalyze_h
#include "simodo/lsp-server/ReportCombiner.h"

#include <algorithm>
#include <mutex>

namespace simodo::lsp
{
    void ReportCombiner::report(const inout::SeverityLevel level,
                        const inout::Location & location,
                        const std::string & briefly,
                        const std::string & atlarge)
    {
        static std::mutex report_mutex;
        std::lock_guard   locker(report_mutex);

        if (!briefly.empty() && briefly[0] == '#')
            ;
        else if ( _messages.end() != std::find_if(_messages.begin(), _messages.end(),
                                    [location,briefly](const MessageFullContent & cont){
                                        return cont.location.uri() == location.uri()
                                        && cont.briefly == briefly;
                                    }))
            return;

        _messages.push_back({level,location,briefly,atlarge});
    }

}
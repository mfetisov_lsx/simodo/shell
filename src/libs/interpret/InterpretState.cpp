/*
MIT License

Copyright (c) 2022 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo
*/

#include "simodo/interpret/InterpretState.h" 

namespace simodo::interpret
{

    // std::u16string getInterpretStateName(InterpretState state)
    // {
    //     std::u16string s;

    //     switch(state)
    //     {
    //     case InterpretState::Regular:
    //         s = u"Regular";
    //         break;
    //     case InterpretState::Unsupported:
    //         s = u"Unsupported";
    //         break;
    //     case InterpretState::Error:
    //         s = u"Error";
    //         break;
    //     case InterpretState::Break:
    //         s = u"Break";
    //         break;
    //     case InterpretState::Continue:
    //         s = u"Continue";
    //         break;
    //     case InterpretState::Return:
    //         s = u"Return";
    //         break;
    //     case InterpretState::Throw:
    //         s = u"Throw";
    //         break;
    //     default: // default: нужен на случай расширения перечисления, чтобы видеть ошибку
    //         s = UNDEF_STRING;
    //         break;
    //     }

    //     return s;
    // }

}
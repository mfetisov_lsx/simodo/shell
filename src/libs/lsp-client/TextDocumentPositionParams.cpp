#include "simodo/lsp-client/TextDocumentPositionParams.h"

#include "simodo/inout/convert/functions.h"

namespace simodo::lsp
{
bool parseTextDocumentIdentifier(const variable::Value & textDocument_value, 
                                 TextDocumentIdentifier & textDocument)
{
    if (textDocument_value.type() != variable::ValueType::Object) 
        return false;

    const variable::Value & uri_value = textDocument_value.getObject()->find(u"uri");
    if (uri_value.type() != variable::ValueType::String)
        return false;

    textDocument.uri = inout::toU8(uri_value.getString());
    
    return !textDocument.uri.empty();
}

bool parseTextDocumentPositionParams(const variable::Value & params_value, 
                                     TextDocumentPositionParams & documentPosition)
{
    if (params_value.type() != variable::ValueType::Object) 
        return false;

    std::shared_ptr<variable::Object> params_object = params_value.getObject();

    if (!parseTextDocumentIdentifier(params_object->find(u"textDocument"), documentPosition.textDocument))
        return false;

    if (!parsePosition(params_object->find(u"position"), documentPosition.position))
        return false;

    return true;
}

}

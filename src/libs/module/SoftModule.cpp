/*
MIT License

Copyright (c) 2021 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo/loom
*/

#include "simodo/module/SoftModule.h"
#include "simodo/LibVersion.h"

namespace simodo::module
{
    version_t SoftModule::version() const 
    {
        return lib_version();
    }

    variable::Object SoftModule::instantiate(std::shared_ptr<variable::Module_interface> /*module_object*/)
    {
        return _soft_module->copy();
    }
}

/*
MIT License

Copyright (c) 2021 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo/stars
*/

#include "simodo/module/HardModule.h"

#include <cassert>

namespace simodo::module
{
    HardModule::HardModule(std::function<ExtModuleFactory_t> creator, interpret::Interpret_interface * interpret)
    : _creator(creator) 
    , _interpret(interpret)
    {
        assert(creator);
        _modules.push_back(creator(interpret));
        assert(_modules.back());
    }

    version_t HardModule::version() const 
    { 
        return _modules[0]->version(); 
    }

    variable::Object HardModule::instantiate(std::shared_ptr<Module_interface> /*module_object*/)
    {
        if (_modules.size() == 1) {
            _modules.push_back(std::shared_ptr<Module_interface>());
            return _modules[0]->instantiate(_modules[0]);
        }

        size_t last_module_index = _modules.size() - 1;
        _modules[last_module_index] = _creator(_interpret);
        _modules.push_back(std::shared_ptr<Module_interface>());
        return _modules[last_module_index]->instantiate(_modules[last_module_index]);
    }
}

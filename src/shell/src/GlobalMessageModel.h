#ifndef GlobalMessageModel_H
#define GlobalMessageModel_H

#include "simodo/shell/access/Access_interface.h"

#include <QtCore>
#include <QtGui>

namespace shell = simodo::shell;

class GlobalMessageModel : public QAbstractListModel
{
    Q_OBJECT

public:
    struct Item
    {
        QString                text;
        shell::MessageSeverity severity;
    };

    struct Palette
    {
        struct Icons
        {
            QIcon debug, info, warning, error;
        };
        struct Foregrounds
        {
            QColor debug, info, warning, error;
        };

        Icons icons;
        Foregrounds foregrounds;
    };

    const int MAX_N_ITEMS;

    GlobalMessageModel(int max_n_items);

    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

    void append(const QList<Item> & append_messages);
    void setPalette(const Palette & palette);

private:
    QQueue<Item> _items;
    Palette _palette;
};

// #include "simodo/edit/shell/Access_interface.h"

// #include <QWidget>
// #include <QStandardItemModel>

// #include <map>
// #include <vector>
// #include <list>
// #include <functional>

// QT_BEGIN_NAMESPACE
// class QVBoxLayout;
// class QListView;
// class QPalette;
// class QCheckBox;
// QT_END_NAMESPACE

// class MainWindow;

// namespace shell = simodo::edit::shell;

// struct MessageAndSeverity
// {
//     QString                text;
//     shell::MessageSeverity severity;
// };

// typedef std::function<bool(size_t button_number)> onClickFunction;

// struct AskButton
// {
//     QString                title;
//     QString                description;
//     onClickFunction        on_click_function = nullptr;
// };

// struct GlobalAsk
// {
//     shell::MessageSeverity severity;
//     QString                title;
//     QString                description;
//     std::vector<AskButton> buttons;
//     QString                group_name;
// };

// class GlobalMessages : public QWidget
// {
//     Q_OBJECT

//     MainWindow &        _main_window;

//     QStandardItemModel  _model;

//     QVBoxLayout *       _layout;
//     QListView *         _list;
//     QWidget *           _ask_widget = nullptr;
//     QCheckBox *         _auto_repeat;

//     QColor              _debug_foreground, _info_foreground, _warning_fForeground, _error_foreground;
//     QIcon               _debug_icon,       _info_icon,       _warning_icon,        _error_icon;

//     std::vector<MessageAndSeverity> _message_buffer;
//     int                 _delay_timer = -1;

//     std::list<GlobalAsk> _ask_queue;
//     std::map<QString,size_t> _group_action;

// public:
//     GlobalMessages(MainWindow & main_window);

//     void sendGlobal(const QString & text, shell::MessageSeverity severity=shell::MessageSeverity::Info);
//     void askGlobal(shell::MessageSeverity severity, const QString & title, const QString & description, 
//                    std::vector<AskButton> buttons, const QString & group_name = "");

// protected:
//     virtual void timerEvent(QTimerEvent *event) override;
//     virtual bool event(QEvent * event) override;

// private:
//     QString setupItemAndGetPrefix(QStandardItem *item, shell::MessageSeverity severity);
//     void setPaletteColors(const QPalette & palette);
//     void flushBuffer();
//     void addItemToModel(const QString & text, shell::MessageSeverity severity);
//     void resetTimer();
//     void showAsk();
//     void closeAsk();
// };

#endif // GlobalMessageModel_H
#ifndef RunManagement_H
#define RunManagement_H

#include "simodo/shell/access/RunnerManagement_interface.h"
#include "simodo/shell/access/AdaptorModeling_interface.h"
#include "simodo/shell/runner/Runner_interface.h"

#include <QtCore>

namespace shell = simodo::shell;

class MainWindow;

class RunManagement : public QObject,
                      public shell::RunnerManagement_interface
{
    Q_OBJECT

    MainWindow &                    _main_window;
    std::atomic<shell::RunnerState> _state = shell::RunnerState::Stoped;

    QString                         _last_runner_name;
    QString                         _last_file_name;
    shell::Runner_interface *       _last_runner = nullptr;

    QWidget *                       _run_options_widget = nullptr;

public:
    RunManagement() = delete;
    RunManagement(MainWindow & main_window);
    ~RunManagement();

    shell::RunnerState state() const { return _state; }
    bool isRunnable(const QString & path);
    bool canRunAutomatic();
    void updateRunnerCombo(const QString & file_name);

public slots:
    bool startModeling(const QString & path);
    bool stopModeling();
    bool pauseModeling();
    void updateRunnerWidget(int index);
    void checkAutoRun(bool checked);

public:
    virtual void started() override;
    virtual void paused() override;
    virtual void stoped() override;
    virtual void received(const QString & text) override;
    virtual void send(const QString & text) override;
    virtual void notifyContentChanged(const QString & path_to_file) override;

private:
    void notifyAboutStateChanges(shell::RunnerState state);

    shell::Runner_interface * _findRunner();
    shell::Runner_interface * _findRunnerOrLast();
};

#endif // RunManagement

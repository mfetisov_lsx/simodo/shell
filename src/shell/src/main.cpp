#include <QApplication>
#include <QCommandLineParser>
#include <QCommandLineOption>
#include <QTranslator>
#include <QLibraryInfo>

#include "MainWindow.h"

#include "simodo/shell/Version.h"

int main(int argc, char *argv[])
{
    Q_INIT_RESOURCE(simodo_shell);

    QApplication app(argc, argv);
    QCoreApplication::setApplicationName("SIMODO shell");
    QCoreApplication::setOrganizationName("BMSTU");
    QCoreApplication::setApplicationVersion(QString::number(static_cast<int>(simodo::shell::Version_Major)));
    QCommandLineParser parser;
    parser.setApplicationDescription("SIMODO shell");
    parser.addHelpOption();
    parser.addVersionOption();
    parser.addPositionalArgument("file", "The file to open.");
    parser.process(app);

    QStringList argFiles;
    for (const QString & fileName : parser.positionalArguments())
        argFiles.push_back(QFileInfo(fileName).canonicalFilePath());

    QTranslator translator;
    if (translator.load(QLocale(), QLatin1String("simodo-shell"), QLatin1String("_"), QCoreApplication::applicationDirPath()))
        QCoreApplication::installTranslator(&translator);

    // qt translation for default dialogs (QFileDialog) and so on
    QTranslator qtTranslator2;
    qtTranslator2.load("qt_" + QLocale::system().name(), QLibraryInfo::location(QLibraryInfo::TranslationsPath));
    app.installTranslator(&qtTranslator2);

    MainWindow mainWin;
    mainWin.show();
    for (const QString &fileName : argFiles)
        mainWin.openFile(fileName);
    return app.exec();
}

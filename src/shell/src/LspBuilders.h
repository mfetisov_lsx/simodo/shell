#ifndef shell_LspBuilders_H
#define shell_LspBuilders_H

#include "simodo/shell/access/LspStructures.h"

QVector<simodo::shell::Diagnostic> builtDiagnostics(const QJsonArray & diagnostics_array, int & checksum);

simodo::lsp::Position       builtPosition(const QJsonObject & position_object);
simodo::lsp::Range          builtRange(const QJsonObject & range_object);


#endif // shell_LspBuilders_H

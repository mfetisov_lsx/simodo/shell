#!/bin/bash

# -extra-plugins=
# renderers,geometryloaders // 3d rendering
# platformthemes,platforminputcontexts // plasma integration
# iconengines // svg
# platforms // platforms?
# xcbglintegrations // graphics?
# styles // breeze
# kf5,kf5/kio // open file dialog
linuxdeployqt $1 -unsupported-allow-new-glibc -bundle-non-qt-libs -extra-plugins=platformthemes,platforminputcontexts,iconengines,platforms,xcbglintegrations,styles,kf5,kf5/kio \
    $(for i in plugins modules semantics; do echo $(echo $(ls $i) | sed 's/\([^[:space:]]\+\.out\)//g ; s/\(visual-calculator[^[:space:]]\+\)//g ; s/\([^[:space:]]\+\)/-executable='$i'\/\1/g' -); done) \
    && echo "successfully deployed"

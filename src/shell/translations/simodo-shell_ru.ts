<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU">
<context>
    <name>MainWindow</name>
    <message>
        <source>File loaded</source>
        <translation>Файл загружен</translation>
    </message>
    <message>
        <source>&amp;%1 %2</source>
        <translation>&amp;%1 %2</translation>
    </message>
    <message>
        <source>File saved</source>
        <translation>Файл сохранён.</translation>
    </message>
    <message>
        <source>About MDI</source>
        <translation>О программе MDI</translation>
    </message>
    <message>
        <source>The &lt;b&gt;MDI&lt;/b&gt; example demonstrates how to write multiple document interface applications using Qt.</source>
        <translation>Пример &lt;b&gt;MDI&lt;/b&gt; демонстрирует, как писать несколько приложений интерфейса документов с помощью Qt.</translation>
    </message>
    <message>
        <source>%1 %2</source>
        <translation>%1 %2</translation>
    </message>
    <message>
        <source>&amp;File</source>
        <translation>&amp;Файл</translation>
    </message>
    <message>
        <source>File</source>
        <translation type="vanished">Файл</translation>
    </message>
    <message>
        <source>&amp;New</source>
        <translation>&amp;Новый</translation>
    </message>
    <message>
        <source>Create a new file</source>
        <translation>Создать новый файл</translation>
    </message>
    <message>
        <source>&amp;Open...</source>
        <translation>&amp;Открыть</translation>
    </message>
    <message>
        <source>Open an existing file</source>
        <translation>Открытие существующего файла</translation>
    </message>
    <message>
        <source>&amp;Save</source>
        <translation>&amp;Сохранить</translation>
    </message>
    <message>
        <source>Save the document to disk</source>
        <translation>Сохранить документ на диск</translation>
    </message>
    <message>
        <source>Save &amp;As...</source>
        <translation>Сохранить &amp;как...</translation>
    </message>
    <message>
        <source>Save the document under a new name</source>
        <translation>Сохранить документ под новым именем</translation>
    </message>
    <message>
        <source>Recent...</source>
        <translation>Недавно посещённые</translation>
    </message>
    <message>
        <source>Switch layout direction</source>
        <translation type="vanished">Переключить направление макета</translation>
    </message>
    <message>
        <source>E&amp;xit</source>
        <translation>&amp;Выход</translation>
    </message>
    <message>
        <source>Exit the application</source>
        <translation>Выйти из приложения</translation>
    </message>
    <message>
        <source>&amp;Edit</source>
        <translation>&amp;Правка</translation>
    </message>
    <message>
        <source>Edit</source>
        <translation type="vanished">Изменить</translation>
    </message>
    <message>
        <source>Cu&amp;t</source>
        <translation>Вы&amp;резать</translation>
    </message>
    <message>
        <source>Cut the current selection&apos;s contents to the clipboard</source>
        <translation>Вырезать содержимое выделенного фрагмента в буфер обмена</translation>
    </message>
    <message>
        <source>&amp;Copy</source>
        <translation>Копировать</translation>
    </message>
    <message>
        <source>Copy the current selection&apos;s contents to the clipboard</source>
        <translation>Скопировать содержимое выделенного фрагмента в буфер обмена</translation>
    </message>
    <message>
        <source>&amp;Paste</source>
        <translation>&amp;Вставить</translation>
    </message>
    <message>
        <source>Paste the clipboard&apos;s contents into the current selection</source>
        <translation>Вставить содержимое буфера обмена в выделенный фрагмент</translation>
    </message>
    <message>
        <source>&amp;Window</source>
        <translation>&amp;Окно</translation>
    </message>
    <message>
        <source>Cl&amp;ose</source>
        <translation>Cl&amp;ose</translation>
    </message>
    <message>
        <source>Close the active window</source>
        <translation>Закрыть активное окно.</translation>
    </message>
    <message>
        <source>Close &amp;All</source>
        <translation>Закрыть &amp;все</translation>
    </message>
    <message>
        <source>Close all the windows</source>
        <translation>Закрыть все окна?</translation>
    </message>
    <message>
        <source>&amp;Tile</source>
        <translation>&amp;Плитка</translation>
    </message>
    <message>
        <source>Tile the windows</source>
        <translation>Плитка окон</translation>
    </message>
    <message>
        <source>&amp;Cascade</source>
        <translation>&amp;Каскад</translation>
    </message>
    <message>
        <source>Cascade the windows</source>
        <translation>Каскадируйте окна</translation>
    </message>
    <message>
        <source>Ne&amp;xt</source>
        <translation>Ne&amp;xt</translation>
    </message>
    <message>
        <source>Move the focus to the next window</source>
        <translation>Переместить фокус в следующее окно</translation>
    </message>
    <message>
        <source>Pre&amp;vious</source>
        <translation>Pre&amp;vious</translation>
    </message>
    <message>
        <source>Move the focus to the previous window</source>
        <translation>Переместить фокус в предыдущее окно</translation>
    </message>
    <message>
        <source>&amp;Help</source>
        <translation>&amp;Справка</translation>
    </message>
    <message>
        <source>&amp;About</source>
        <translation>&amp;О программе</translation>
    </message>
    <message>
        <source>Show the application&apos;s About box</source>
        <translation>Показать диалог о программе</translation>
    </message>
    <message>
        <source>About &amp;Qt</source>
        <translation>О &amp;Qt</translation>
    </message>
    <message>
        <source>Show the Qt library&apos;s About box</source>
        <translation>Показать диалог о библиотеке Qt</translation>
    </message>
    <message>
        <source>Ready</source>
        <translation>Готово</translation>
    </message>
    <message>
        <source>Предупреждение: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Ошибка: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>All documents saved</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Print Document</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Export PDF</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Exported &quot;%1&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Unable to create document for extension %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>File extension %1 not supported</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Save All</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Save all documents</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Print...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Print Preview...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Export PDF...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Run</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Execute current edit text</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Stop</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Terminate current execution</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;View</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Status bar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Show or hide status bar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Global &amp;Messages</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Global messages</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Home directory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Recent home directory...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Set recent home directory...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Set &amp;home...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Set home/working directory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Show or hide global messages panel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Quick actions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Scanning directory &apos;%1&apos; for plugins by mask &apos;%2&apos;...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1 plugins found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Plugin &apos;%1&apos; was successfully loaded</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Some errors occurred during plugin &apos;%1&apos; load</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>

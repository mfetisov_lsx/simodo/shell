Иконки для оболочек GNU/Linux используются из текущей темы оболочки (проверено на теме Breeze).

Для Windows используются иконки из ресурсов программы, т.е. из каталога images. В этом каталоге иконки именуются в соответствии с именем иконки темы. В качестве источника иконок для ресурсов Windows используется ресурс [https://iconarchive.com/show/oxygen-icons-by-oxygen-icons.org.html](https://iconarchive.com/show/oxygen-icons-by-oxygen-icons.org.html).

Просьба брать иконки оттуда для единообразия стиля.

/*
MIT License 

Copyright (c) 2021 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов, 

https://bmstu.codes/lsx/simodo/loom
*/

#ifndef simodo_parser_InputStreamSupplier_interface
#define simodo_parser_InputStreamSupplier_interface

/*! \file InputStreamSupplier_interface.h
    \brief Интерфейс поставщика входных потоков.
    
*/

#include "simodo/inout/token/InputStream_interface.h"

#include <memory>

namespace simodo::inout
{
    /**
     * @brief Интерфейс поставщика входных потоков.
     * 
     */
    class InputStreamSupplier_interface
    {
    public:
        virtual ~InputStreamSupplier_interface() = default; ///< Виртуальный деструктор

        virtual std::shared_ptr<InputStream_interface> supply(const std::string & path) = 0;
    };

}

#endif // simodo_parser_InputStreamSupplier_interface

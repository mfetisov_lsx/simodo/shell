/*
MIT License 

Copyright (c) 2021 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов, 

https://bmstu.codes/lsx/simodo/loom
*/

#ifndef simodo_inout_Parser_interface
#define simodo_inout_Parser_interface

/*! \file Parser_interface.h
    \brief Интерфейс парсера.
    
*/

// #include "simodo/parser/SyntaxDataCollector_interface.h"

#include "simodo/inout/token/InputStream_interface.h"

namespace simodo::inout
{
    /**
     * @brief Интерфейс парсера.
     * 
     */
    class Parser_interface
    {
    public:
        virtual ~Parser_interface() = default; ///< Виртуальный деструктор

        virtual bool parse() = 0;
        virtual bool parse(inout::InputStream_interface & stream) = 0;
    };

}

#endif // simodo_inout_Parser_interface

/*
MIT License 

Copyright (c) 2021 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов, 

https://bmstu.codes/lsx/simodo
*/

#ifndef simodo_variable_json_parser_Json5Rdp
#define simodo_variable_json_parser_Json5Rdp

/*! \file Json5Rdp.h
    \brief Загрузка (разбор) JSON5 из файла или потока во внутренний формат класса variable::Value
*/

#include "simodo/variable/Variable.h"
#include "simodo/variable/json/parser/JsonAnalyzeDataBuilder_interface.h"

#include "simodo/inout/reporter/Reporter_abstract.h"
#include "simodo/inout/token/Parser_interface.h"
#include "simodo/inout/token/InputStream_interface.h"
#include "simodo/inout/token/Tokenizer.h"
#include "simodo/inout/token/RdpBaseSugar.h"

#include <string>
#include <memory>

namespace simodo::variable
{
    class Json5Rdp : public inout::Parser_interface, public inout::RdpBaseSugar
    {
        inout::uri_set_t                    _files;
        JsonAnalyzeDataBuilder_interface &  _builder;
        const std::string                   _json_file;
        Value &                             _value;
        mutable std::unique_ptr<inout::Tokenizer> _tokenizer;
        mutable bool                        _ok = true;

    public:
        Json5Rdp(inout::Reporter_abstract & m, const std::string json_file, Value & value); 
        Json5Rdp(inout::Reporter_abstract & m, JsonAnalyzeDataBuilder_interface & builder, const std::string json_file, Value & value); 

        virtual bool    parse() override;
        virtual bool    parse(inout::InputStream_interface & stream) override;

    protected:
        void    parseValue(const inout::Token & value_token, Value & value) const;
        void    parseObject(const inout::Token & open_brace_token, Value & value) const;
        void    parseArray(const inout::Token & open_brace_token, Value & value) const;
    };
}

#endif // simodo_variable_json_parser_Json5Rdp

/*
MIT License 

Copyright (c) 2021 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов, 

https://bmstu.codes/lsx/simodo
*/

#ifndef simodo_variable_json_parser_JsonLspBuilder
#define simodo_variable_json_parser_JsonLspBuilder

/*! \file JsonLspBuilder.h
    \brief Загрузка (разбор) JSON из файла или потока во внутренний формат класса variable::Value
*/

#include "simodo/variable/json/parser/JsonAnalyzeDataBuilder_interface.h"

#include <string>
#include <vector>

namespace simodo::variable
{
    class JsonLspBuilder : public JsonAnalyzeDataBuilder_interface
    {
        std::vector<inout::Token> &  _const_variable_set;
        std::vector<std::pair<inout::Token,inout::Token>> &
                                            _group_set;

    public:
        JsonLspBuilder(std::vector<inout::Token> &  const_variable_set,
                       std::vector<std::pair<inout::Token,inout::Token>> & group_set)
            : _const_variable_set(const_variable_set)
            , _group_set(group_set)
        {}
        
        virtual void addGroup(const inout::Token & open_brace_token, 
                              const inout::Token & close_brace_token) override 
        {
            _group_set.push_back({open_brace_token, close_brace_token});
        }

        virtual void addVariable(const inout::Token & variable_token) override 
        {
            _const_variable_set.push_back( {{variable_token.lexeme(),inout::LexemeType::Id},
                                            variable_token.token(),
                                            variable_token.location(),
                                            variable_token.qualification(),
                                            variable_token.context()});
        }

        virtual void addConst(const inout::Token & const_token) override 
        {
            _const_variable_set.push_back(const_token);
        }
    };

}

#endif // simodo_variable_json_parser_JsonLspBuilder

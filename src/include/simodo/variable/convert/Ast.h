/*
MIT License 

Copyright (c) 2021 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов, 

https://bmstu.codes/lsx/simodo
*/

#ifndef simodo_ast_serialization_AstSerialization
#define simodo_ast_serialization_AstSerialization

/*! \file AstSerialization.h
    \brief Сериализация структуры абстрактного синтаксического дерева.
*/

#include "simodo/ast/Tree.h"
#include "simodo/variable/json/Serialization.h"


namespace simodo::variable
{
    Value toValue(const ast::Tree & tree);
    Value toValue(const inout::uri_set_t & files);
    Value toValue(const ast::Node & node);
    Value toValue(const inout::Token & token);
    Value toValue(const inout::TokenLocation & location);
    Value toValue(const inout::Range & range);
    Value toValue(const inout::Position & position);
    Value toValue(const std::vector<ast::Node> & branches);

    /// \todo Нужно сделать!
    // ast::Tree   convertToTree(const Value & json_value);

}

#endif // simodo_ast_serialization_AstSerialization

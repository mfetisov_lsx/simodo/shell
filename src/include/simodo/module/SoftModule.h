/*
MIT License

Copyright (c) 2021 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo
*/

#ifndef simodo_module_SoftModule
#define simodo_module_SoftModule

/*! \file Module_interface.h
    \brief Интерфейс управляющего кода модуля
*/

#include "simodo/variable/Module_interface.h"

#include <memory>

namespace simodo::module
{
    class SoftModule : public variable::Module_interface
    {
        std::shared_ptr<variable::Object>   _soft_module;

    public:
        SoftModule() = delete;
        SoftModule(std::shared_ptr<variable::Object> soft_module) 
            : _soft_module(soft_module)
        {}

        virtual version_t version() const override;
        virtual variable::Object instantiate(std::shared_ptr<variable::Module_interface> module_object) override;
    };

}

#endif // simodo_module_SoftModule

/*
MIT License

Copyright (c) 2021 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo/stars
*/

#ifndef SIMODO_module_ModuleCollector_interface
#define SIMODO_module_ModuleCollector_interface

/*! \file ModuleCollector_interface.h
    \brief Интерфейс управления модулями.
*/

#include "simodo/variable/Variable.h"

namespace simodo::interpret
{
    class Interpret_interface;
}

namespace simodo::module
{
    class ModuleCollector_interface
    {
    public:
        virtual ~ModuleCollector_interface() = default;

        virtual std::shared_ptr<variable::Array>  produceObjectsByMask(const std::string & module_mask) = 0;
        virtual std::shared_ptr<variable::Object> produceObject(const std::string & module_name,
                                                        interpret::Interpret_interface * interpret = nullptr) = 0;
        virtual variable::Value     invoke(variable::Object & object, const std::string & method_name, const variable::VariableSet_t & args) = 0;
        virtual std::string         last_error() const = 0;
    };

}

#endif // SIMODO_module_ModuleCollector_interface

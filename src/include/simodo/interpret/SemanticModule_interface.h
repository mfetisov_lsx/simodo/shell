/*
MIT License

Copyright (c) 2021 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo
*/

#ifndef simodo_interpret_SemanticModule_interface
#define simodo_interpret_SemanticModule_interface

/*! \file SemanticModule_interface.h
    \brief Интерфейс держателя семантических правил
*/

#include "simodo/interpret/InterpretState.h"
#include "simodo/interpret/InterpretType.h" 
#include "simodo/ast/Node.h"

#include <vector>
#include <string>

namespace simodo::interpret
{
    class Interpret_interface;

    /*!
     * \brief The SemanticModule_interface class
     */
    class SemanticModule_interface
    {
    public:
        virtual ~SemanticModule_interface() = default;

        virtual void    setInterpret(Interpret_interface * inter) = 0;

        virtual bool    checkSemanticName(const std::u16string & semantic_name) const = 0;
        virtual bool    checkInterpretType(InterpretType interpret_type) const = 0;

        virtual InterpretState   before_start() = 0;
        virtual InterpretState   before_finish(InterpretState state) = 0;

        virtual bool    isOperationExists(ast::OperationCode operation_code) const = 0;
        virtual InterpretState   performOperation(const ast::Node & op) = 0;

        virtual void    reset() = 0;

    };

    typedef std::vector<SemanticModule_interface *> SemanticModules;

}

#endif // simodo_interpret_SemanticModule_interface

/*
MIT License

Copyright (c) 2021 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo
*/

#ifndef simodo_interpret_Parallelize_interface
#define simodo_interpret_Parallelize_interface

/*! \file Parallelize_interface.h
    \brief .
*/

#include "simodo/interpret/StackOfNames_interface.h"

#include "simodo/variable/Variable.h"

#include <memory>

namespace simodo::interpret
{
    class Interpret_interface;

    struct ChildFlowInfo
    {
        std::shared_ptr<variable::Object> our_object;
        Interpret_interface *             child;
        boundary_index_t                  child_bound;
    };

    class Parallelize_interface
    {
    public:
        virtual ~Parallelize_interface() = default;

        virtual bool                   addChildFiber(const variable::Variable & var)    = 0;
        virtual bool                   push(const variable::Variable & var)             = 0;
        virtual bool                   pull(variable::Variable & var)                   = 0;
        virtual bool                   wait(const variable::Variable & var)             = 0;

        virtual const ChildFlowInfo *  findChildFlowInfo(const variable::Variable & var)  const = 0;
    };
}

#endif // simodo_interpret_Parallelize_interface

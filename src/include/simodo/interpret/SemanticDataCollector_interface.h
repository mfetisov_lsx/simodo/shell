/*
MIT License

Copyright (c) 2022 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo
*/

#ifndef simodo_interpret_SemanticDataCollector_interface
#define simodo_interpret_SemanticDataCollector_interface

/*! \file SemanticDataCollector_interface.h
    \brief Интерфейс сбора семантических данных в процессе семантического анализа
*/

#include "simodo/parser/SyntaxDataCollector_interface.h"

#include "simodo/variable/Variable.h"

namespace simodo::interpret
{
    class SemanticDataCollector_interface : public parser::SyntaxDataCollector_interface
    {
    public:
        virtual void collectNameDeclared(const variable::Variable & variable) = 0;
        virtual void collectNameInitiated(const variable::Variable & variable) = 0;
        virtual void collectNameAssigned(const variable::Variable & variable, const inout::TokenLocation & location) = 0;
        virtual void collectNameUsed(const variable::Variable & variable, const inout::TokenLocation & location) = 0;
        virtual void collectScoped(const inout::TokenLocation & begin, const inout::TokenLocation & end) = 0;
        virtual void collectRef(const inout::Token & token, const std::u16string & ref) = 0;
        virtual void collectRemoteFunctionLaunch(const inout::TokenLocation & location) = 0;
    };

    class SemanticDataCollector_null : public SemanticDataCollector_interface
    {
    public:
        virtual void collectToken(const inout::Token & ) override {}

        virtual void collectNameDeclared(const variable::Variable & ) override {}
        virtual void collectNameInitiated(const variable::Variable & ) override {}
        virtual void collectNameAssigned(const variable::Variable & , const inout::TokenLocation & ) override {}
        virtual void collectNameUsed(const variable::Variable & , const inout::TokenLocation & ) override {}
        virtual void collectScoped(const inout::TokenLocation & , const inout::TokenLocation & ) override {}
        virtual void collectRef(const inout::Token & , const std::u16string & ) override {}
        virtual void collectRemoteFunctionLaunch(const inout::TokenLocation & ) override {}
    };

}

#endif // simodo_interpret_SemanticDataCollector_interface

/*
MIT License

Copyright (c) 2023 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo
*/

#ifndef simodo_interpret_HostOperationLoader
#define simodo_interpret_HostOperationLoader

/*! \file HostOperationLoader.h
    \brief .
*/

#include "simodo/variable/Variable.h"


namespace simodo::interpret
{
    variable::VariableSet_t loadSemanticOperationsEnums(const std::string & grammar_dir);

}

#endif // simodo_interpret_HostOperationLoader

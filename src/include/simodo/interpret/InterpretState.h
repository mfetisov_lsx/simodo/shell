/*
MIT License

Copyright (c) 2021 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo
*/

#ifndef simodo_interpret_InterpretState
#define simodo_interpret_InterpretState

/*! \file InterpretState.h
    \brief Состояния разбора абстрактного дерева операционной семантики.
*/

#include "simodo/loom/FiberStatus.h"

#include <vector>
#include <string>

namespace simodo::interpret
{
    inline const std::u16string UNDEF_STRING = u"(UNDEF)";  ///< Строка для неопределённых величин  

    /// \todo По идее нужно реализовать отличный от loom::FiberStatus.
    /// Дело в том, что возврат loom::FiberStatus из нити не корректно, т.к. имеет другой смысл:
    /// Возврат представляет запрос на смену статуса Станка, а не сам статус!
    /// Однако и то, что было ранее совершенно не годится. Нужен новый перечисляемый тип.

    typedef loom::FiberStatus InterpretState;

    /*!
     * \brief Результат разбора семантического дерева
     */
    // enum class InterpretState
    // {
    //     // Regular,        ///< Штатная работа
    //     // Unsupported,    ///< Операция не поддерживается
    //     // Error,          ///< Возникла ошибка
    //     // Break,          ///< Обработка операции break
    //     // Continue,       ///< Обработка операции continue
    //     // Return,         ///< Обработка операции return
    //     // Throw,          ///< Обработка операции throw
    // };

    /*!
     * \brief Преобразование кода состояние обработки фрагмента кода в строку
     * \param state Код состояние обработки фрагмента кода
     * \return Строка с наименованием кода состояние обработки фрагмента кода
     */
    // std::u16string getInterpretStateName(InterpretState state);

}

#endif // simodo_interpret_InterpretState

/*
MIT License

Copyright (c) 2022 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo/loom
*/

#ifndef simodo_interpret_host_base_SemanticsHost_abstract
#define simodo_interpret_host_base_SemanticsHost_abstract

/*! \file BaseInterpret_abstract.h
    \brief Класс разбора SBL операции, заданной в виде ноды семантического дерева (ast::Node)
*/

#include "simodo/interpret/Interpret.h"
#include "simodo/parser/fuze/BaseOperationCode.h"
#include "simodo/variable/Variable.h"
#include "simodo/interpret/builtins/hosts/base/BaseOperationParser.h"
#include "simodo/interpret/builtins/hosts/base/BaseMachine.h"
// #include "simodo/interpret/SemanticModule_interface.h"


namespace simodo::interpret::builtins
{
    class BaseInterpret_abstract: public SemanticModule_interface
    {
        Interpret_interface *               _interpret;
        BaseMachine                         _machine;
        std::vector<BaseOperationParser_t>  _operation_switchboard;

    public:
        BaseInterpret_abstract() = delete;
        BaseInterpret_abstract(Interpret_interface * inter);

        virtual ~BaseInterpret_abstract();

    public:
        Interpret_interface & inter()       { return *_interpret; }
        BaseMachine &   machine()           { return _machine; }

    public:
        virtual void    setInterpret(Interpret_interface * inter)       override { _interpret = inter; }
        virtual void    reset()                                         override;
        virtual InterpretState   before_start()                         override;
        virtual bool    isOperationExists(ast::OperationCode operation_code) const override;
        virtual InterpretState   performOperation(const ast::Node & op) override;
        virtual bool    checkSemanticName(const std::u16string & semantic_name) const override 
                        { return semantic_name == parser::SCRIPT_HOST_NAME; }
        virtual InterpretState   before_finish(InterpretState state)    override;

    public:
        virtual void importNamespace(std::u16string name, variable::Object ns, inout::TokenLocation location);

    // Группа методов непосредственно отвечающих за выполнение уже разобранной и подготовленной операции.
    // Именно тут вызываются методы machine.
    // Различные типы интерпретаторов могут перехватывать (перегружать) эти методы для выполнения 
    // специальных действий.

    public:
        virtual InterpretState   executeNone             ();
        virtual InterpretState   executePushConstant     (const inout::Token & constant_value);
        virtual InterpretState   executePushValue        (const inout::Token & variable_name);
        virtual InterpretState   executeObjectElement    (const inout::Token & dot, const inout::Token & variable_name);
        virtual InterpretState   executeFunctionCall     (const ast::Node & op);
        virtual InterpretState   executeProcedureCheck   ();
        virtual InterpretState   executeBlock            (const ast::Node & op);
        virtual InterpretState   executePop              ();
    };
}

#endif // simodo_interpret_host_base_SemanticsHost_abstract

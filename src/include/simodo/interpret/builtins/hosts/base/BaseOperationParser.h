/*
MIT License

Copyright (c) 2019 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo/loom
*/

#ifndef simodo_interpret_host_base_OperationParser
#define simodo_interpret_host_base_OperationParser

/*! \file BaseOperationParser.h
    \brief .
*/

#include "simodo/ast/Node.h"
#include "simodo/interpret/InterpretState.h"

#include <string>

namespace simodo::interpret::builtins
{
    class BaseInterpret_abstract;

    typedef InterpretState (*BaseOperationParser_t) (BaseInterpret_abstract &, const ast::Node &);

    namespace BaseOperationParser
    {
        InterpretState   parseNone               (BaseInterpret_abstract &host, const ast::Node & op);
        InterpretState   parsePushConstant       (BaseInterpret_abstract &host, const ast::Node & op);
        InterpretState   parsePushVariable       (BaseInterpret_abstract &host, const ast::Node & op);
        InterpretState   parseObjectElement      (BaseInterpret_abstract &host, const ast::Node & op);
        InterpretState   parseFunctionCall       (BaseInterpret_abstract &host, const ast::Node & op);
        InterpretState   parseProcedureCheck     (BaseInterpret_abstract &host, const ast::Node & op);
        InterpretState   parseBlock              (BaseInterpret_abstract &host, const ast::Node & op);
        InterpretState   parsePop                (BaseInterpret_abstract &host, const ast::Node & op);
    }

}

#endif // simodo_interpret_host_base_OperationParser

/*
MIT License 

Copyright (c) 2021 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов, 

https://bmstu.codes/lsx/simodo/loom
*/

#ifndef simodo_fuze_StBuilderModule
#define simodo_fuze_StBuilderModule

/*! \file AstFormationModule.h
    \brief Модуль-построитель семантического дерева (только для внутреннего использования)
*/

#include "simodo/variable/Module_interface.h"
#include "simodo/ast/generator/FormationWrapper.h"
#include "simodo/inout/token/Token.h"

#include <string>
#include <memory>

namespace simodo::interpret::builtins
{
    class AstFormationModule: public variable::Module_interface
    {
        variable::VariableSet_t &         _hosts;
        ast::FormationWrapper             _helper;

        const inout::Token *              _current_production = nullptr;
        const std::vector<inout::Token> * _current_pattern    = nullptr;
        uint16_t                          _current_stream_no  = ast::DEFAULT_STREAM_NO;

    public:
        AstFormationModule();
        AstFormationModule(variable::VariableSet_t & hosts);

        ast::FormationFlow_interface &  helper() { return _helper; }
        ast::FormationFlow_interface &  operator () () { return _helper; }

        const inout::Token &        current_production()    const { return *_current_production; }
        const std::vector<inout::Token> & current_pattern() const { return *_current_pattern; }
        uint16_t                    current_stream_no()     const { return _current_stream_no; }

        void    setCurrentProduction(const inout::Token * t)    { _current_production = t; }
        void    setCurrentPattern(const std::vector<inout::Token> * pattern) { _current_pattern = pattern; }
        void    setCurrentStreamNo(uint16_t no) { _current_stream_no = no; }

    public:
        virtual version_t           version()               const override { return lib_version(); }
        virtual variable::Object    instantiate(std::shared_ptr<variable::Module_interface> module_object) override;
    };

}

#endif // simodo_fuze_SemanticTreeBuilder


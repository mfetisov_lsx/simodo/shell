/*
MIT License

Copyright (c) 2022 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo/loom
*/

#ifndef simodo_loom_Loom_interface
#define simodo_loom_Loom_interface

/** 
 * @file Loom_interface.h
 * @brief Определения для Loom_interface
 */

#include "simodo/loom/Fiber_interface.h"

#include <string>
#include <vector>

namespace simodo::loom 
{
    typedef uint32_t fiber_no_t;

    struct FiberStructure
    {
        Fiber_interface * p_fiber;
        Fiber_interface * p_parent;
        bool              need_delete;
        FiberStatus       status;
        fiber_no_t        no;
        std::string       name;
        const Fiber_interface * p_waiting_for = nullptr;
    };

    /**
     * @brief Материя мира всегда в движении! 
     * 
     * @note Доктор Борменталь: "Постойте, профессор... Неужели это пул потоков? Не верю!"
     * 
     * @note Профессор Преображенский: "Любопытно. Продолжайте наблюдение, коллега."
     * 
     */
    class Loom_interface
    {
    public:
        virtual ~Loom_interface() = default;

        virtual bool                dock(Fiber_interface * p_fiber, 
                                        bool need_delete=false, 
                                        Fiber_interface * p_parent=nullptr,
                                        FiberStatus status=FiberStatus::Delayed,
                                        const std::string & name={})    = 0;
        virtual bool                stretch(Fiber_interface * p_fiber)  = 0;
        virtual bool                pause()                             = 0;
        virtual bool                resume()                            = 0;
        virtual bool                stop()                              = 0;
        virtual void                finish()                            = 0;

        virtual bool                paused()                 const      = 0;
        virtual size_t              shuttles()               const      = 0;
        virtual Fiber_interface *   causer()                            = 0;
        virtual std::vector<FiberStructure> fibers()         const      = 0;

    };

}

#endif // simodo_loom_Loom_interface

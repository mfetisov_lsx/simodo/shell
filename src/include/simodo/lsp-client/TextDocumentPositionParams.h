#ifndef SIMODO_LSP_TextDocumentPositionParams_h
#define SIMODO_LSP_TextDocumentPositionParams_h

#include "simodo/lsp-client/Location.h"

namespace simodo::lsp
{
    /**
     * Text documents are identified using a URI. On the protocol level, URIs are passed as strings.
    */
    struct TextDocumentIdentifier
    {
        /**
         * The text document's URI.
         */
        std::string uri;
    };

    /**
     * A parameter literal used in requests to pass a text document and a position inside 
     * that document. It is up to the client to decide how a selection is converted into a 
     * position when issuing a request for a text document. The client can for example 
     * honor or ignore the selection direction to make LSP request consistent with features 
     * implemented internally.
    */
    struct TextDocumentPositionParams
    {
        /**
         * The text document.
         */
        TextDocumentIdentifier textDocument;

        /**
         * The position inside the text document.
         */
        Position position;
    };

    bool parseTextDocumentIdentifier(const simodo::variable::Value & textDocument_value, 
                                     TextDocumentIdentifier & textDocument);
    bool parseTextDocumentPositionParams(const simodo::variable::Value & params, 
                                     TextDocumentPositionParams & documentPosition);
}
#endif // SIMODO_LSP_TextDocumentPositionParams_h
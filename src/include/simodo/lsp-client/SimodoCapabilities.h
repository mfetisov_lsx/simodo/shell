#ifndef _simodo_lsp_SimodoCapabilities_h
#define _simodo_lsp_SimodoCapabilities_h

#include <string>
#include <vector>

namespace simodo::lsp
{

struct SimodoRunnerOption
{
	std::u16string 	languageID;
	std::u16string	viewName;
};

struct SimodoCapabilities
{
	bool			runnerProvider	= false;
	bool			debugProvider   = false;
	std::vector<SimodoRunnerOption>	runnerOptions;
};

}

#endif //_simodo_lsp_SimodoCapabilities_h

#ifndef SIMODO_LSP_Location_h
#define SIMODO_LSP_Location_h

#include "simodo/variable/Variable.h"

#include <string>

namespace simodo::lsp
{
    /**
     * Position in a text document expressed as zero-based line and zero-based character offset. 
     * A position is between two characters like an ‘insert’ cursor in an editor. 
     * Special values like for example -1 to denote the end of a line are not supported.
    */
    typedef inout::Position Position;

    /**
     * A range in a text document expressed as (zero-based) start and end positions. 
     * A range is comparable to a selection in an editor. Therefore, the end position 
     * is exclusive. If you want to specify a range that contains a line including the 
     * line ending character(s) then use an end position denoting the start of the 
     * next line.
    */
    typedef inout::Range    Range;

    /**
     * Represents a location inside a resource, such as a line inside a text file.
    */
    typedef inout::Location Location;

    bool parsePosition(const variable::Value & position_value, inout::Position & position);
    bool parseRange(const variable::Value & range_value, inout::Range & range);
    bool parseLocation(const variable::Value & location_value, inout::Location & location);
    
}


#endif // SIMODO_LSP_Location_h
#ifndef SIMODO_LSP_MessageFullContent_h
#define SIMODO_LSP_MessageFullContent_h

#include "simodo/inout/reporter/Reporter_abstract.h"

namespace simodo::lsp
{

struct MessageFullContent
{
    inout::SeverityLevel level;
    inout::Location      location;
    std::string          briefly;
    std::string          atlarge;
};

}

#endif // SIMODO_LSP_MessageFullContent_h
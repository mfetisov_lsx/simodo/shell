#ifndef SIMODO_LSP_ReportCombiner_h
#define SIMODO_LSP_ReportCombiner_h

#include "simodo/lsp-server/MessageFullContent.h"

#include <vector>

namespace simodo::lsp
{

class ReportCombiner: public inout::Reporter_abstract
{
    std::vector<MessageFullContent> _messages;

public:
    virtual void report(const inout::SeverityLevel level,
                        const inout::Location & location,
                        const std::string & briefly,
                        const std::string & atlarge) override;

    const std::vector<MessageFullContent> & messages() { return _messages; }
};

}

#endif // SIMODO_LSP_ReportCombiner
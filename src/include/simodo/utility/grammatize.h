/*
MIT License

Copyright (c) 2021 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo
*/

#ifndef simodo_utility_grammatize
#define simodo_utility_grammatize

/*! \file generateDotFile.h
    \brief Формирование DOT-файла абстрактного дерева операционной семантики. 
*/

#include "simodo/parser/Grammar.h"
#include "simodo/inout/reporter/Reporter_abstract.h"
#include "simodo/inout/token/InputStream_interface.h"

#include <ostream>
#include <string>

namespace simodo::utility
{
    void printRules(const parser::Grammar & g, 
                    const inout::uri_set_t & files, 
                    bool need_st_info, 
                    std::ostream & out);

    void printStateTransitions(const parser::Grammar & g, std::ostream & out);

    void createStateTransitionsGraph(const parser::Grammar & g, 
                                     const std::string & dot_file, 
                                     const std::string & grammar_file, 
                                     bool need_silence, 
                                     std::ostream & out);

    bool grammatize(const std::string & grammar_file,
                inout::InputStream_interface & in,
                std::ostream & out,
                inout::Reporter_abstract & m,
                const std::string & json_file_name,
                const std::string & st_dot_file_name,
                parser::TableBuildMethod grammar_builder_method,
                const std::string & dot_file_name,
                bool need_state_transitions_info,
                bool need_rules_info,
                bool need_st_info,
                bool need_time_intervals,
                bool need_silence,
                bool need_build_grammar,
                bool need_load_grammar,
                bool need_analyze_handles,
                bool need_analyze_inserts,
                parser::Grammar & grammar);

}

#endif // simodo_utility_grammatize

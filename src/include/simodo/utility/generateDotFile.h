/*
MIT License

Copyright (c) 2021 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo
*/

#ifndef simodo_utility_generateDotFile
#define simodo_utility_generateDotFile

/*! \file generateDotFile.h
    \brief Формирование DOT-файла абстрактного дерева операционной семантики. 
*/

#include "simodo/ast/Node.h"

#include <map>

namespace simodo::utility
{
    void generateDotFile(const std::string & dot_file_name,
                         const ast::Node & node,
                         const std::map<ast::OperationCode,std::string> & excluded_nodes);

}

#endif // simodo_utility_generateDotFile

/*
MIT License

Copyright (c) 2021 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo
*/

#ifndef simodo_utility_common_functions
#define simodo_utility_common_functions

/*! \file common_functions.h
    \brief Часто используемые функции в утилитах проекта SIMODO
*/

#include "simodo/inout/token/Lexeme.h"
#include "simodo/ast/Node.h"

#include <map>

namespace simodo::utility
{
    std::string getMnemonic(const inout::Lexeme & lex, bool abstract_lexeme=true);

    void printSemanticTree(const ast::Node & ast, const inout::uri_set_t & files, int level=0, int shift=0);

    void replaceAll(std::string& str, const std::string& from, const std::string& to);

    std::string toHtml(std::string text);

}

#endif // simodo_utility_common_functions

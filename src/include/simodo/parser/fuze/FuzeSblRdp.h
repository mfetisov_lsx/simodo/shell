/*
MIT License 

Copyright (c) 2021 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов, 

https://bmstu.codes/lsx/simodo/loom
*/

#ifndef simodo_parser_fuze_FuzeSubsetSblRdp
#define simodo_parser_fuze_FuzeSubsetSblRdp

/*! \file FuzeSblRdp.h
    \brief Разбор некоторых операторов на подмножестве языка SBL методом рекурсивного спуска
*/

#include "simodo/parser/SyntaxDataCollector_interface.h"

#include "simodo/ast/generator/FormationFlow_interface.h"
#include "simodo/inout/token/Tokenizer.h"
#include "simodo/inout/token/RdpBaseSugar.h"
#include "simodo/inout/reporter/Reporter_abstract.h"


namespace simodo::parser
{
    /*!
     * \brief Класс начального разбора примитивов ScriptC методом рекурсивного спуска (recursive descent parser, RDP)
     *
     */
    class FuzeSblRdp : public inout::RdpBaseSugar
    {
        ast::FormationFlow_interface &  _flow;
        SyntaxDataCollector_interface & _syntax_data_collector;

    public:
        FuzeSblRdp() = delete; ///< Пустой конструктор не поддерживается!

        FuzeSblRdp(inout::Reporter_abstract & m, 
                   ast::FormationFlow_interface & flow,
                   SyntaxDataCollector_interface & syntax_data_collector)
            : inout::RdpBaseSugar(m, flow.tree().files())
            , _flow(flow)
            , _syntax_data_collector(syntax_data_collector)
        {}

        bool    parse(inout::Tokenizer & tzer, inout::Token & t);

    protected:

        bool    parseProcedureCalling(inout::Tokenizer & tzer, inout::Token & t);

        bool    parseAddress(inout::Tokenizer & tzer, inout::Token & t);

        bool    parseAddressAtom(inout::Tokenizer & tzer, inout::Token & t);

        bool    parseArgumentList(inout::Tokenizer & tzer, inout::Token & t);

        /*!
         * \brief Геттер на вспомогательный класс для построения АСД
         * \return Ссылка на вспомогательный класс для построения АСД
         */
        ast::FormationFlow_interface & ast() { return _flow; }

        inout::Token   getToken(inout::Tokenizer & tzer) const ;
    };
}

#endif // simodo_parser_fuze_FuzeSubsetSblRdp

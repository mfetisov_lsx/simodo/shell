/*
MIT License 

Copyright (c) 2021 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов, 

https://bmstu.codes/lsx/simodo/loom
*/

#ifndef simodo_parser_fuze_fuze_keywords
#define simodo_parser_fuze_fuze_keywords

/*! \file fuze_keywords.h
    \brief Ключевые слова грамматики FUZE.
*/

#include <string>

namespace simodo::parser
{
    inline const std::u16string ID_STRING {u"ID"};
    inline const std::u16string NUMBER_STRING {u"NUMBER"};
    inline const std::u16string ANNOTATION_STRING {u"ANNOTATION"};

}

#endif // simodo_parser_fuze_fuze_keywords

/*
MIT License

Copyright (c) 2022 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo
*/

#ifndef simodo_parser_SyntaxDataCollector_interface
#define simodo_parser_SyntaxDataCollector_interface

/*! \file SyntaxDataCollector_interface.h
    \brief Интерфейс сбора синтаксических данных в процессе синтаксического разбора
*/

#include "simodo/inout/token/Token.h"

namespace simodo::parser
{
    class SyntaxDataCollector_interface
    {
    public:
        virtual ~SyntaxDataCollector_interface() = default;

        virtual void collectToken(const inout::Token & token) = 0;
    };

    class SyntaxDataCollector_null : public SyntaxDataCollector_interface
    {
    public:
        virtual void collectToken(const inout::Token & ) override {}
    };

}

#endif // simodo_parser_SyntaxDataCollector_interface

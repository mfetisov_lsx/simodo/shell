/*
MIT License 

Copyright (c) 2021 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов, 

https://bmstu.codes/lsx/simodo/loom
*/

#ifndef simodo_fuze_AstFormationWrapper
#define simodo_fuze_AstFormationWrapper

/*! \file FormationWrapper.h
    \brief Класс формирования семантического дерева
*/

#include "simodo/ast/generator/FormationFlow.h"

#include <string>
#include <map>


namespace simodo::ast
{
    inline const uint16_t DEFAULT_STREAM_NO = 55;

    /*!
     * \brief Вспомогательный класс построителя абстрактного синтаксического дерева
     */
    class FormationWrapper : public FormationFlow_interface
    {
        Tree                                _tree;
        FormationFlow                       _main_stream;
        std::map<uint16_t,FormationFlow>    _ast_streams;
        uint16_t                            _current_stream_no = DEFAULT_STREAM_NO;

    public:
        void                    setStreamNo(uint16_t no) { _current_stream_no = no; }
        uint16_t                getStreamNo() const { return _current_stream_no; }

        const FormationFlow &   getStream(uint16_t no);
        void                    removeStream(uint16_t no);

    public:
        virtual void            addNode(const std::u16string & host, 
                                        OperationCode op, 
                                        const inout::Token & op_symbol, 
                                        const inout::Token & bound) override;
        virtual void            addNode_StepInto(const std::u16string & host, 
                                        OperationCode op, 
                                        const inout::Token & op_symbol, 
                                        const inout::Token & bound) override;
        virtual bool            goParent() override;
        virtual void            addFile(const std::string & file_path) override;
        virtual const Tree &    tree() const override;
        virtual void            finalize() override;

    protected:
        FormationFlow &         getStream_mutable(uint16_t no);
    };

}

#endif // simodo_fuze_AstFormationWrapper

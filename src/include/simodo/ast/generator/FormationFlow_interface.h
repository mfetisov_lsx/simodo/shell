/*
MIT License 

Copyright (c) 2021 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов, 

https://bmstu.codes/lsx/simodo/loom
*/

#ifndef simodo_ast_generator_AstFormationFlow_interface
#define simodo_ast_generator_AstFormationFlow_interface

/*! \file FormationFlow_interface.h
    \brief Интерфейс построения АДОС в стиле перемещения указателя в формируемом потоке.
*/

#include "simodo/ast/Tree.h"

#include <string>
#include <map>
#include <set>


namespace simodo::ast
{
    /**
     * @brief Интерфейс построения АДОС в стиле перемещения указателя в формируемом потоке.
     * 
     */
    class FormationFlow_interface
    {
    public:
        virtual ~FormationFlow_interface() = default;

        virtual void            addNode(const std::u16string & host, 
                                        OperationCode op, 
                                        const inout::Token & op_symbol, 
                                        const inout::Token & bound) = 0;
        virtual void            addNode_StepInto(const std::u16string & host, 
                                        OperationCode op, 
                                        const inout::Token & op_symbol, 
                                        const inout::Token & bound) = 0;
        virtual bool            goParent() = 0;
        virtual void            addFile(const std::string & file_path) = 0;
        virtual const Tree &    tree() const = 0;
        virtual void            finalize() = 0;
    };

    /**
     * @brief Пустая реализация FormationFlow_interface
     * 
     */
    class AstFormationFlow_null : public FormationFlow_interface
    {
        Tree _tree;

    public:
        virtual void            addNode(const std::u16string &, OperationCode , const inout::Token &, const inout::Token &) final override {}
        virtual void            addNode_StepInto(const std::u16string &, OperationCode , const inout::Token &, const inout::Token &) final override {}
        virtual bool            goParent() final override { return true; }
        virtual void            addFile(const std::string & ) final override {}
        virtual const Tree &    tree() const final override { return _tree; }
        virtual void            finalize() final override {}
    };

}

#endif // simodo_ast_generator_AstFormationFlow_interface

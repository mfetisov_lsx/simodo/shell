/*
MIT License 

Copyright (c) 2021 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов, 

https://bmstu.codes/lsx/simodo
*/

#ifndef simodo_ast_NodeFriend
#define simodo_ast_NodeFriend

/*! \file NodeFriend.h
    \brief Класс для изменения содержимого узла семантического дерева.
*/

#include "simodo/ast/Node.h"

namespace simodo::ast
{
    /*!
     * \brief Класс для изменения содержимого узла семантического дерева
     *
     */
    class NodeFriend {
    public:
        static void addNode(Node & parent, const std::u16string & host, int op, const inout::Token & op_symbol, const inout::Token & bound) {
            parent._branches.emplace_back(host, op, op_symbol, bound);
        }

        static void insertBranchBegin(Node & parent, const std::vector<Node> & other_branch) {
            parent._branches.insert(parent._branches.begin(), other_branch.begin(), other_branch.end());
        }

        static void insertBranchEnd(Node & parent, const std::vector<Node> & other_branch) {
            parent._branches.insert(parent._branches.end(), other_branch.begin(), other_branch.end());
        }

        static std::vector<Node> & branches(Node & parent) {
            return parent._branches;
        }

        static OperationCode & operation(Node & parent) { return parent._operation; }
    };

}

#endif // simodo_ast_NodeFriend

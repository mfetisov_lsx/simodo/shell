#ifndef simodo_shell_LspData_H
#define simodo_shell_LspData_H

#include "simodo/lsp-client/Location.h"
#include "simodo/lsp-client/LspEnums.h"
#include "simodo/lsp-client/CompletionItemKind.h"

#include <QJsonDocument>
#include <QString>
#include <QVector>

#include <string>
#include <vector>

namespace simodo::shell
{

typedef QString URI;

struct CodeDescription
{
    URI href;
};

struct Diagnostic
{
    lsp::Range          range;
    lsp::DiagnosticSeverity severity; 
    // QString             code;
    // CodeDescription     codeDescription;
    // QString             source;
    QString             message;
    // tags?: DiagnosticTag[];
    // relatedInformation?: DiagnosticRelatedInformation[];
    // data?: unknown;
};

struct DocumentSymbol
{
    QString                 name, detail;
    lsp::SymbolKind         kind;
    lsp::Range              range, selectionRange;
    // QVector<documentSymbol> children;
};

struct SemanticToken
{
    int                         line, startChar, length;
    std::u16string              tokenType;
    std::vector<std::u16string> tokenModifiers;
};

struct CompletionItemLabelDetails
{
    QString detail;
    QString description;
};

struct CompletionItem
{
    QString                     label;
    CompletionItemLabelDetails  labelDetails;
    lsp::CompletionItemKind     kind;
};

}

#endif // simodo_shell_LspData_H

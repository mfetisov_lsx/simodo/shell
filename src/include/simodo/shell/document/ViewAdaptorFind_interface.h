/**
 * @file ViewAdaptorFind_interface.h
 * @author Фетисов Михаил Вячеславович (fetisov.michael@yandex.ru)
 * @brief Объявление интерфейса поиска.
 * @version 0.1
 * @date 2023-04-16
 *
 * @copyright MIT: Michael Fetisov, Anton Bushev
 *
 */
#ifndef simodo_shell_document_ViewAdaptorFind_interface_H
#define simodo_shell_document_ViewAdaptorFind_interface_H

#include <QTextDocument>

namespace simodo::shell
{

/**
 * @brief Интерфейс поиска для адаптера представления документа.
 * 
 * @details
 * Используется при запросе со стороны оболочки через метод ViewAdaptor_interface::getFindInterface.
 *
 */
class ViewAdaptorFind_interface
{
public:
    virtual ~ViewAdaptorFind_interface() = default;

    /**
     * @brief Запрос текста для вставки в качестве образца для поиска и замены
    */
    virtual QString getSampleTextToFind() const = 0;

    /**
     * @brief Поиск элемента по заданной строке
     * 
     * @param str строка для поиска
     * @param options параметры поиска
     */
    virtual bool find(const QString & str, QTextDocument::FindFlags options) = 0;

    /**
     * @brief Замена элемента текста what на элемент to
     * 
     * @param what строка для поиска
     * @param to   строка для замены
     * @param options параметры поиска
     * @param all признак замены всех подходящих элементов
    */
    virtual bool replace(const QString & what, const QString & to, QTextDocument::FindFlags options, bool all) = 0;
};

}

#endif // simodo_shell_document_ViewAdaptorFind_interface_H
/**
 * @file DocumentAdaptor_interface.h
 * @author Фетисов Михаил Вячеславович (fetisov.michael@yandex.ru)
 * @brief Объявления интерфейса адаптера документа.
 * @version 0.1
 * @date 2023-02-15
 *
 * @copyright MIT: Michael Fetisov, Anton Bushev
 *
 */
#ifndef simodo_shell_document_DocumentAdaptor_interface_H
#define simodo_shell_document_DocumentAdaptor_interface_H

#include "simodo/shell/Service.h"

#include <QString>

namespace simodo::shell
{

/**
 * @brief Интерфейс адаптера документа, унифицирующий взаимодействие оболочки с файлом документа.
 *
 * @details
 * Адаптер документа обеспечивает хранение содержимого файла в виде, подготовленном для отображения
 * и изменения с помощью представления документа.
 *
 * Оболочка хранит адаптеры документов в ассоциации с полными путями соответствующих файлов. После
 * закрытия окна документа, его адаптер остаётся на хранении в оболочке, но может быть удалён ею при
 * необходимости.
 *
 * @attention Адаптер документа не должен сам ассоциироваться с путём файла, это поддерживается оболочкой.
 *
 * @attention Адаптер документа не должен иметь пользовательского интерфейса.
 *
 * Плагинам, как панелей, так и документов, целесообразно работать с файлами через оболочку, запрашивая
 * у неё адаптеры документов через метод shell::Access_interface::getDocumentAdaptor, т.к. соответствующие
 * файлы могут быть открыты оболочкой и изменены пользователем. Этот механизм нужно использовать, например,
 * при работе с языковыми серверами
 * (см. <a href="https://en.wikipedia.org/wiki/Language_Server_Protocol">LSP</a>).
 *
 * Адаптер документа также позволяет обмениваться содержимым между разными представлениями документов.
 * Регламент обмена пока не определён, предполагается простой текст.
 *
 */
class DocumentAdaptor_interface
{
public:
    virtual ~DocumentAdaptor_interface() = default;

    /**
     * @brief Загрузка содержимого файл по заданному пути в адаптер документа.
     *
     * @param path полный путь к файлу
     * @return текст ошибки или пустая строка.
     */
    virtual QString loadFile(const QString & path) = 0;

    /**
     * @brief Документ был сохранён
     * 
     * @details После последнего сохранения устанавливается флаг, который проверяется в данном методе.
     * После вызова этого метода флаг сбрасывается.
     * 
     * @return документ только что был сохранён.
    */
    virtual bool wasSaved() = 0;

    /**
     * @brief Сохранение содержимого адаптера документа в файл по указанному пути.
     *
     * @param path полный путь к файлу
     * @return текст ошибки или пустая строка.
     */
    virtual QString saveFile(const QString & path) = 0;

    /**
     * @brief Получение содержимого адаптера документа в виде текста.
     *
     * @return содержимое адаптера документа
     */
    virtual QString text() = 0;
    QString text() const
    {
        auto thi = const_cast<DocumentAdaptor_interface *>(this);
        return thi->text();
    }

    /**
     * @brief Загрузка текста в адаптер документа.
     *
     * @param text текст
     * @return true - текст удалось загрузить,
     * @return false - текст не удалось загрузить
     */
    virtual bool setText(const QString & text) = 0;

    /**
     * @brief Возврат признака изменения документа.
     *
     * @return true - документ был изменён,
     * @return false - документ не был изменён.
     */
    virtual bool isModified() = 0;

    /**
     * @brief Установка признака изменения документа.
     *
     * @param modified признак изменения документа
     */
    virtual void setModified(bool modified=true) = 0;
};

}

#endif // simodo_shell_document_DocumentAdaptor_interface_H
/**
 * @file Runner_plugin.h
 * @author Фетисов Михаил Вячеславович (fetisov.michael@yandex.ru)
 * @brief Объявления интерфейса плагина модуля моделирования
 * @version 0.1
 * @date 2023-05-06
 *
 * @copyright MIT: Michael Fetisov, Anton Bushev
 *
 */
#ifndef simodo_shell_runner_Runner_plugin_H
#define simodo_shell_runner_Runner_plugin_H

#include "simodo/shell/Plugin_interface.h"
#include "simodo/shell/access/Access_interface.h"

#include <QObject>

namespace simodo::shell
{
class Runner_interface;

/**
 * @brief Интерфейс плагина моделирующего модуля.
 * 
 * @details
 * Концептуально плагины выполняют только функцию динамически загружаемой фабрики и 
 * могут только создавать другие объекты и предоставлять общее описание создаваемых 
 * объектов. Плагин моделирующего модуля является вырожденным случаем плагина, т.к. создаёт
 * единственный объект только один раз - метод createRunner вызывается оболочкой единожды.
 *
 */
class Runner_plugin : public Plugin_interface
{
public:
    /**
     * @brief Создание объекта моделирующего модуля
     * 
     * @param shell_access ссылка на интерфейс функций оболочки
     * @return указатель на созданный моделирующий модуль
     */
    virtual Runner_interface * createRunner(Access_interface & shell_access) = 0;
};

}

#define RunnerPlugin_iid "BMSTU.SIMODO.shell.plugin.Runner"

Q_DECLARE_INTERFACE(simodo::shell::Runner_plugin, RunnerPlugin_iid)

#endif // simodo_shell_runner_Runner_plugin_H

/**
 * @file Runner_interface.h
 * @author Фетисов Михаил Вячеславович (fetisov.michael@yandex.ru)
 * @brief Объявление интерфейса модуля моделирования.
 * @version 0.1
 * @date 2023-05-06
 * 
 * @copyright MIT: Michael Fetisov, Anton Bushev
 * 
 */
#ifndef simodo_shell_runner_Runner_interface_H
#define simodo_shell_runner_Runner_interface_H

#include <QString>

QT_BEGIN_NAMESPACE
class QWidget;
QT_END_NAMESPACE

namespace simodo::shell
{
class Runner_plugin;

/**
 * @brief Интерфейс моделирующего модуля
 * 
 * @details
 * Процесс моделирования может находится в трёх состояниях: завершённом (или не выполняемом), 
 * выполняемом и приостановленном.
 * 
 * Оболочка поддерживает следующий порядок переходов между состояниями:
 * 
 * @dot
 * digraph modeling_states {
 *  rankdir = LR;
 *  Stoped     [shape=oval];
 *  Paused     [shape=oval];
 *  Running    [shape=oval];
 *  Stoped  -> {Running};
 *  Running -> {Paused, Stoped};
 *  Paused  -> {Running, Stoped};
 * }
 * @enddot
 *
 * @attention
 * Методы данного интерфейса: startModeling, pauseModeling и stopModeling не оказывают влияние на состояние 
 * моделирования в оболочке. Для изменения состояния моделирования в оболочке, необходимо вызывать 
 * соответствующие методы интерфейса RunnerManagement_interface, который можно получить с помощью
 * вызова Access_interface::getRunnerManagement.
 * 
 */
class Runner_interface
{
public:
    virtual ~Runner_interface() = default;

    /**
     * @brief Запрос указателя на плагин
     * 
     * @details
     * Концептуальный метод. Оболочка запрашивает указателя на фабрику (плагин и есть фабрика), 
     * чтобы проверить общие описания для данного объекта. Пока не используется, зарезервирован на 
     * возможное использование в будущем.
     * 
     * @return Указатель на плагин
     */
    virtual Runner_plugin * plugin() = 0;

    /**
     * @brief Наименование моделирующего модуля. Используется оболочкой для отображения при выборе.
     * 
     * @return Наименование моделирующего модуля
     */
    virtual QString name() const = 0;

    /**
     * @brief Описание моделирующего модуля. Используется оболочной для отображения короткой подсказки,
     * например, при наведении на название модуля мышкой.
     * 
     * @return QString 
     */
    virtual QString description() const = 0;

    /**
     * @brief Проверка возможности выполнения моделирования (запуска) файла по указанному пути
     * 
     * @param path путь к файлу
     * @return true - моделирующий модуль может выполнить запуск указанного файла, 
     * @return false - моделирующий модуль не поддерживает работу с указанным файлом
     */
    virtual bool isRunnable(const QString & path) = 0;

    /**
     * @brief Запрос на выполнение моделирования (на исполнение) заданного файла.
     * 
     * Вызов метода может быть сделан только, если моделирование не запущено или приостановлено.
     * 
     * @note
     * Если метод возвращает true - это не значит, что моделирование на самом деле начало выполняться.
     * Возврат успеха означает, что процесс моделирования инициирован. Состояние выполнения в оболочке не 
     * зависит от значения возвращаемого этим методом. 
     * 
     * Чтобы изменить состояние выполнения в оболочке, необходимо вызвать метод 
     * RunnerManagement_interface::started. RunnerManagement_interface нужно запросить в оболочке, 
     * вызвав метод Access_interface::getRunnerManagement.
     * 
     * @param path путь к файлу
     * @return true - запуск моделирования инициирован (но не обязательно будет выполнен), 
     * @return false - запуск моделирования отклонён
     */
    virtual bool startModeling(const QString & path) = 0;

    /**
     * @brief Запрос на приостановку моделирования.
     * 
     * Может быть вызван оболочкой только, если моделирование находится в состоянии выполнения.
     * 
     * @return true - приостановка моделирования инициирована (но не обязательно будет выполнена)
     * @return false - приостановка моделирования отклонена
     */
    virtual bool pauseModeling() = 0;

    /**
     * @brief Запрос на остановку моделирования.
     * 
     * Метод может быть вызван оболочкой только, если моделирование выполняется или приостановлено.
     * 
     * @return true - остановка моделирования инициирована (но не обязательно будет выполнена)
     * @return false - остановка моделирования отклонена
     */
    virtual bool stopModeling() = 0;

    /**
     * @brief Передача управляющих данных в процесс моделирования.
     * 
     * Метод может быть вызван оболочкой только в состоянии выполнения моделирования.
     * 
     * Формат передаваемых данных не регламентирован.
     * 
     * @param data управляющие данные
     * @return true - если передача данных в процесс моделирования была инициирована,  
     * @return false - если передача данных была отклонена
     */
    virtual bool send(const QString & data) = 0;

    /**
     * @brief Текст ошибки 
     * 
     * @return текст ошибки или пустая строка, если ошибки не было
     */
    virtual QString error_text() = 0;

    /**
     * @brief Дать виджет для размещения на панели инструментов оболочки
     * 
     * @return Указатель на виджет
    */
    virtual QWidget * getToolbarWidget() = 0;

    /**
     * @brief Проверка на активность автозапуска раннера
    */
    virtual bool isAutoRunActive() const = 0;

    /**
     * @brief Установка режима автозапуска раннера
    */
    virtual void setAutoRunActive(bool is_active) = 0;
};

}

#endif // simodo_shell_runner_Runner_interface_H
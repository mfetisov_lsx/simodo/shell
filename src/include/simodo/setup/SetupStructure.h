/**
 * @file SetupStructure.h
 * @author Michael Fetisov (fetisov.michael@bmstu.ru)
 * @brief 
 * @version 0.1
 * @date 2022-08-26
 * 
 * @copyright Copyright (c) 2022
 * 
 * 
 */

#pragma once

#include "simodo/variable/Variable.h"

#include <string>

namespace simodo::setup
{
    struct SetupStructure
    {
        std::string     id;
        std::string     name;
        std::string     brief;
        std::string     description;
        variable::Value value;
    };

    struct SetupDescription
    {
        std::string                     brief,
                                        description;
        std::vector<SetupStructure>     setup;
    };

}

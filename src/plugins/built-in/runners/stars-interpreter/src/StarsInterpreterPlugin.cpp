#include "StarsInterpreterPlugin.h"
#include "StarsInterpreter.h"

using namespace simodo;

StarsInterpreterPlugin::StarsInterpreterPlugin()
{
}

shell::Runner_interface * StarsInterpreterPlugin::createRunner(shell::Access_interface & shell_access)
{
    return new StarsInterpreter(this, shell_access);
}

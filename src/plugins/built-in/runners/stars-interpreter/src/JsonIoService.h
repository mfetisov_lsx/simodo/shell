#ifndef JsonIoService_H
#define JsonIoService_H

#include <QtCore/QtCore>

class JsonIoService : public QObject
{
public:
    struct MessageEvent;

    JsonIoService();
    ~JsonIoService();

    void setReciever(QObject * reciever);

    void start(QString program, QStringList program_arguments);
    bool isRunning();
    void stop();

    void write(QJsonDocument message);

protected:
    bool event(QEvent * event) override;

private:
    struct CommandEvent;

    Q_OBJECT

    QProcess _process;
    // @todo атомизировать работу с переменной
    QObject * _reciever;

    bool _messageEvent(MessageEvent * event);
    bool _commandEvent(CommandEvent * event);

    void _start(QString program, QStringList program_arguments);
    void _stop();
    void _close();

    void _write(QJsonDocument message);

    void _startTimer();
    void _stopTimer();

private slots:
    void _recieve();
};

struct JsonIoService::MessageEvent : public QEvent
{
    static const QEvent::Type TYPE;
    QVector<QJsonDocument> messages;
    MessageEvent(QVector<QJsonDocument> messages);
};

#endif // JsonIoService_H
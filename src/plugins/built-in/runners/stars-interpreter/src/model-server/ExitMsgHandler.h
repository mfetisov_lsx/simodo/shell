#ifndef ExitMsgHandler_H
#define ExitMsgHandler_H

#include "TypedMsgHandler.h"

#include "simodo/shell/access/Access_interface.h"

namespace simodo::edit::plugin::model_server
{
    class ExitMsgHandler : public TypedMsgHandler
    {
    public:
        using ExitCallback = std::function<void()>;

        ExitMsgHandler( simodo::shell::Access_interface & 
                        , ExitCallback exit_callback
                        )
            : TypedMsgHandler("exit")
            // , _access(access)
            , _exit_callback(exit_callback)
        {}
        ~ExitMsgHandler() override = default;

        bool handle(QJsonObject msg) override;

    private:
        // simodo::shell::Access_interface & _access;
        ExitCallback _exit_callback;
    };
} // simodo::edit::plugin::model_server

#endif // ExitMsgHandler_H

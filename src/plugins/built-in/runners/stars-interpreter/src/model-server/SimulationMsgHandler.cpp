#include "SimulationMsgHandler.h"

#include "JsonUtility.h"

using namespace simodo::edit::plugin::model_server;

bool SimulationMsgHandler::handle(QJsonObject msg)
{
    if (!TypedMsgHandler::handle(msg))
    {
        return false;
    }

    auto level = JsonUtility::findString(msg, "level");

    auto message_severity = shell::MessageSeverity::Info;
    if (level == "warning")
    {
        message_severity = shell::MessageSeverity::Warning;
    }
    else if (level == "error" || level == "fatal")
    {
        message_severity = shell::MessageSeverity::Error;
    }

    auto briefly = JsonUtility::findString(msg, "briefly").value_or("");
    auto atlarge = JsonUtility::findString(msg, "atlarge").value_or("");

    QString message;
    if (!briefly.isEmpty())
    {
        message += briefly;
        if (!atlarge.isEmpty())
        {
            message += ". ";
        }
    }
    message += atlarge;

    _write_callback(message.trimmed(), message_severity);
    return true;
}

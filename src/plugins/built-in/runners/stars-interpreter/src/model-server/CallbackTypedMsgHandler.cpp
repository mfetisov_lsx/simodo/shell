#include "CallbackTypedMsgHandler.h"

using namespace simodo::edit::plugin::model_server;

CallbackTypedMsgHandler::CallbackTypedMsgHandler(QString type, CallbackTypedMsgHandler::Callback callback)
            : TypedMsgHandler(type)
            , _callback(callback)
{}

bool CallbackTypedMsgHandler::handle(QJsonObject msg)
{
    if (!TypedMsgHandler::handle(msg))
    {
        return {};
    }

    return _callback(msg);
}

#ifndef MsgHandler_H
#define MsgHandler_H

#include <QtCore>

namespace simodo::edit::plugin::model_server
{
    class MsgHandler
    {
    public:
        virtual ~MsgHandler() = default;

        virtual bool handle(QJsonObject msg) = 0;
    };
} // simodo::edit::plugin::model_server

#endif // MsgHandler_H

#ifndef SetupWidget_H
#define SetupWidget_H

#include "simodo/shell/access/Access_interface.h"
#include "simodo/setup/Setup.h"

#include <QWidget>

#include <QApplication>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonValue>
#include <QVBoxLayout>
#include <QLabel>
#include <QComboBox>
#include <QLineEdit>
#include <QPushButton>
#include <QFile>
#include <QDebug>
#include <QScrollArea>

namespace shell = simodo::shell;

class SetupWidget : public QWidget
{
    Q_OBJECT

public:
    SetupWidget();
    ~SetupWidget();

protected:

private:
    // QVBoxLayout *mainLayout;
    // QScrollArea *scrollArea;
    // QWidget *scrollWidget;
    QMap<QString, QWidget*> widgetsMap;
    // QPushButton *saveButton;

    // QWidget *descriptionContainer;
    // QWidget *contentContainer;
    // QWidget *saveButtonContainer;

    // simodo::setup::Setup setup;

    void createMainLayout();
    void loadSetup();
    void createDescriptionSection();
    void createContentSections();
    void createSaveButtonSection();
    void connectSaveButton();
    void saveSettings();
    void setLayoutAndScrollArea();
};

#endif // SetupWidget_H

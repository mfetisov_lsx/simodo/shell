#include "GeneralTextPlugin.h"
#include "GeneralTextViewAdaptor.h"


GeneralTextPlugin::GeneralTextPlugin()
{
    _extensions << "txt" << "*";
}

QString GeneralTextPlugin::view_name() const
{
    return tr("Text document");
}

QString GeneralTextPlugin::view_short_name() const
{
    return "text";
}

QString GeneralTextPlugin::id() const
{
    return "general-text";
}

QSet<QString> GeneralTextPlugin::alternate_view_mnemonics(const QString & ) const
{
    return {};
}

std::shared_ptr<shell::DocumentAdaptor_interface> GeneralTextPlugin::createDocumentAdaptor(shell::Access_interface & )
{
    return std::make_shared<GeneralTextDocumentAdaptor>();
}

shell::ViewAdaptor_interface * GeneralTextPlugin::createViewAdaptor(shell::Access_interface & shell_access,
                                                        std::shared_ptr<shell::DocumentAdaptor_interface> document_adaptor,
                                                        const QString & path_to_file)
{
    return new GeneralTextViewAdaptor(shell_access, this, document_adaptor, path_to_file);
}

shell::service_t GeneralTextPlugin::supports() const
{
    return  shell::PS_None
            |  shell::PS_Doc_New
            | shell::PS_Doc_Open
            | shell::PS_Doc_Save
            | shell::PS_Doc_Print
            | shell::PS_Doc_ExportToPdf
            // | shell::PS_Doc_Export
            | shell::PS_Doc_UndoRedo
            | shell::PS_Doc_Copy
            | shell::PS_Doc_Paste
            | shell::PS_Doc_Zoom
            | shell::PS_Doc_Find
            | shell::PS_Doc_Replace
            | shell::PS_Doc_Loadable
            | shell::PS_Doc_Splittable
            | shell::PS_Doc_Run
            ;
}

QString GeneralTextPlugin::icon_theme() const
{
    return "text-plain";
}

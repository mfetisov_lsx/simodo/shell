#ifndef Minimap_H
#define Minimap_H

#include <QPlainTextEdit>

class CodeEdit;
class Highlighter;

class Minimap : public QPlainTextEdit
{
    Q_OBJECT

    CodeEdit *      _edit;
    Highlighter *   _highlighter         = nullptr;

    int             _last_pos           = -1;
    bool            _left_button_pressed = false;

public: 
    Minimap(CodeEdit * edit);

    void highlightViewportArea(const QString & selected_text, QTextDocument::FindFlags find_options);

    void resetHighlighter(Highlighter * highlighter);
    void rehighlight();

signals:
    void requestPosition(int pos);

protected:
    virtual bool event(QEvent * event) override;
    virtual void wheelEvent(QWheelEvent * event) override;
    virtual void contextMenuEvent(QContextMenuEvent *event) override;
    virtual void mousePressEvent(QMouseEvent * event) override;
    virtual void mouseReleaseEvent(QMouseEvent * event) override;
    virtual void mouseMoveEvent(QMouseEvent * event) override;
    virtual void mouseDoubleClickEvent(QMouseEvent * event) override;

private:
    void emitRequestPosition(QPoint point);
};

#endif // Minimap_H

#ifndef GeneralTextDocumentAdaptor_H
#define GeneralTextDocumentAdaptor_H

#include "simodo/shell/document/DocumentAdaptor_interface.h"

#include "simodo/shell/access/LspStructures.h"


#include "QTextDocument"

namespace shell = simodo::shell;

class Highlighter;

class GeneralTextDocumentAdaptor : public shell::DocumentAdaptor_interface
{
    QTextDocument _document;

    Highlighter * _highlighter      = nullptr;
    bool          _save_flag        = false;
    bool          _save_backup_file = false;

    QVector<simodo::shell::Diagnostic>              _diagnostics;
    std::multimap<int,simodo::shell::SemanticToken> _semantic_tokens;

public:
    GeneralTextDocumentAdaptor();
    ~GeneralTextDocumentAdaptor();

    virtual QString loadFile(const QString & fileName) override;
    virtual QString saveFile(const QString & fileName) override;
    virtual bool wasSaved() override;
    virtual QString text() override;
    virtual bool setText(const QString & text) override;
    virtual bool isModified() override;
    virtual void setModified(bool modified) override;

    QTextDocument & document() { return _document; }
    void setHighlighter(Highlighter * highlighter);
    Highlighter * highlighter() { return _highlighter; }

    const QVector<simodo::shell::Diagnostic> & diagnostics() const { return _diagnostics; }
    const std::multimap<int,simodo::shell::SemanticToken> & semantic_tokens() const { return _semantic_tokens; }

    void publishDiagnostics(QVector<simodo::shell::Diagnostic> & diagnostics);
    void semanticTokens(std::multimap<int,simodo::shell::SemanticToken> & tokens);

    void setSaveBackupMode(bool save_backup=true) { _save_backup_file = save_backup; }
};

#endif // GeneralTextDocumentAdaptor_H

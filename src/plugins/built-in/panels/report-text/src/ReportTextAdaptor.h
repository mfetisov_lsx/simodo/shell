#ifndef ReportTextAdaptor_H
#define ReportTextAdaptor_H

#include "simodo/shell/panel/PanelAdaptor_interface.h"
#include "simodo/shell/access/Access_interface.h"

namespace shell  = simodo::shell;

QT_BEGIN_NAMESPACE
class QPlainTextEdit;
QT_END_NAMESPACE

class ReportTextAdaptor : public shell::PanelAdaptor_interface
{
    QPlainTextEdit * _edit;

    QString         _title;

public:
    ReportTextAdaptor(shell::Access_interface & plug_data);

    virtual QWidget * panel_widget() override;
    virtual QString title() const override;
    virtual void reset() override {}
    virtual void changedCurrentDocument(const QString & /*path*/) override {}
    virtual bool activateService(shell::service_t /*service*/) override { return false; }
    virtual bool acceptData(const QJsonObject & data) override;
    virtual shell::AdaptorModeling_interface * getModelingInterface() override { return nullptr; }
};

#endif // ReportTextAdaptor_H

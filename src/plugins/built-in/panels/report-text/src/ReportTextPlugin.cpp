#include "ReportTextPlugin.h"
#include "ReportTextAdaptor.h"

using namespace simodo;

ReportTextPlugin::ReportTextPlugin()
{
}

shell::PanelAdaptor_interface * ReportTextPlugin::createPanelAdaptor(shell::Access_interface & shell_access)
{
    return new ReportTextAdaptor(shell_access);
}

shell::service_t ReportTextPlugin::supports() const
{
    return shell::PS_None 
        | shell::PS_Pan_Singleton
    ;
}

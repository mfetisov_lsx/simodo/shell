#include "ReportTextAdaptor.h"

#include <QPlainTextEdit>
#include <QJsonObject>

ReportTextAdaptor::ReportTextAdaptor(shell::Access_interface & /*shell_access*/)
{
    _edit = new QPlainTextEdit;
    _edit->setReadOnly(true);
}

QWidget* ReportTextAdaptor::panel_widget()
{
    return _edit;
}

QString ReportTextAdaptor::title() const
{
    return _title;
}

bool ReportTextAdaptor::acceptData(const QJsonObject & data)
{
    auto it_text = data.find("text");
    if (it_text == data.end())
        return false;

    auto it_file = data.find("file");
    if (it_file != data.end())
        _title = it_file->toString();

    _edit->setPlainText(it_text->toString());
    return true;
}


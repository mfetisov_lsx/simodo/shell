#ifndef MarkdownViewer_H
#define MarkdownViewer_H

#include <QTextEdit>

QT_BEGIN_NAMESPACE
QT_END_NAMESPACE

class MarkdownViewer : public QTextEdit
{
public:

protected:
    virtual void resizeEvent(QResizeEvent * event) override;
};

#endif // MarkdownViewer_H

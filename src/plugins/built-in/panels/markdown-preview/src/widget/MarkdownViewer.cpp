#include "MarkdownViewer.h"

#include <QTextBlock>

void MarkdownViewer::resizeEvent(QResizeEvent *e)
{
    const QSize & size = e->size();

    for(QTextBlock block = document()->firstBlock(); block.isValid(); block = block.next()) {
        for (QTextBlock::iterator it = block.begin(); !(it.atEnd()); ++it) {
            QTextFragment fragment = it.fragment();

            if (fragment.isValid() && fragment.charFormat().isImageFormat()) {
                QTextImageFormat new_image_format = fragment.charFormat().toImageFormat();

                new_image_format.setWidth(size.width());
                new_image_format.setQuality();

                if (new_image_format.isValid()) {
                    QTextCursor helper = textCursor();

                    helper.setPosition(fragment.position());
                    helper.setPosition(fragment.position() + fragment.length(),
                                        QTextCursor::KeepAnchor);
                    helper.setCharFormat(new_image_format);
                }
            }
        }
    }

    QTextEdit::resizeEvent(e);

    document()->setModified(false);
}



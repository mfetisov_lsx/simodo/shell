#ifndef MarkdownAdaptor_H
#define MarkdownAdaptor_H

#include "simodo/shell/panel/PanelAdaptor_interface.h"
#include "simodo/shell/access/Access_interface.h"

#include "widget/MarkdownViewer.h"

namespace shell  = simodo::shell;

class MarkdownAdaptor : public shell::PanelAdaptor_interface
{
    MarkdownViewer * _edit;

    QString         _title;

public:
    MarkdownAdaptor(shell::Access_interface & plug_data);

    virtual QWidget * panel_widget() override;
    virtual QString title() const override;
    virtual void reset() override {}
    virtual void changedCurrentDocument(const QString & /*path*/) override {}
    virtual bool activateService(shell::service_t /*service*/) override { return false; }
    virtual bool acceptData(const QJsonObject & data) override;
    virtual shell::AdaptorModeling_interface * getModelingInterface() override { return nullptr; }
};

#endif // MarkdownAdaptor_H

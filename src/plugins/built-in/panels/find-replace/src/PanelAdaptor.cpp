#include "PanelAdaptor.h"

PanelAdaptor::PanelAdaptor(shell::Access_interface & shell_access)
{
    _widget = new FindReplaceWidget(this, shell_access);
}

QString PanelAdaptor::title() const
{
    return _widget->replace_mode() ? "Replace" : "Find";
}

void PanelAdaptor::changedCurrentDocument(const QString & )
{
    _widget->resetReplaceMode(_widget->replace_mode());
}

bool PanelAdaptor::activateService(shell::service_t service)
{
    Q_ASSERT(_widget);

    return _widget->resetReplaceMode(shell::check(service,shell::PS_Pan_Replace));
}

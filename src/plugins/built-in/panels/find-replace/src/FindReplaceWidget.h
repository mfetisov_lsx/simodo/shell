#ifndef FindReplaceWidget_H
#define FindReplaceWidget_H

#include "simodo/shell/panel/PanelAdaptor_interface.h"
#include "simodo/shell/access/Access_interface.h"

#include <QWidget>

class QVBoxLayout;
class QLabel;
class QLineEdit;
class QCheckBox;
class QPushButton;

namespace shell  = simodo::shell;

class FindReplaceWidget : public QWidget
{
    Q_OBJECT

    shell::PanelAdaptor_interface * _adaptor;
    shell::Access_interface &       _shell_access;

    QVBoxLayout *   _layout;
    QLabel *        _find_label;
    QLineEdit *     _find_edit;
    QCheckBox *     _sensetive_check;
    QCheckBox *     _words_check;
    // QCheckBox *     _regexp_check;
    QLabel *        _replace_label;
    QLineEdit *     _replace_edit;
    QCheckBox *     _replace_all_check;
    QPushButton *   _find_button;
    QPushButton *   _replace_button;

public:
    FindReplaceWidget(shell::PanelAdaptor_interface *adaptor, shell::Access_interface & plug_data);
    virtual ~FindReplaceWidget() override;

    bool    replace_mode() const;

public slots:
    bool    resetReplaceMode(bool turn_on);
    void    find();
    void    replace();

protected:

};

#endif // FindReplaceWidget_H

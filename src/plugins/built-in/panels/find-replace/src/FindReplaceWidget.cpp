#include <QtWidgets>

#include "FindReplaceWidget.h"

#include "simodo/shell/document/ViewAdaptor_interface.h"
#include "simodo/shell/document/ViewAdaptorFind_interface.h"
#include "simodo/shell/document/Document_plugin.h"
#include "simodo/shell/Service.h"

FindReplaceWidget::FindReplaceWidget(shell::PanelAdaptor_interface *adaptor, shell::Access_interface &shell_access)
    : _adaptor(adaptor), _shell_access(shell_access)
{
    _layout             = new QVBoxLayout(this);
    _find_edit          = new QLineEdit;
    _replace_edit       = new QLineEdit;
    _find_label         = new QLabel(tr("Find text:"));
    _replace_label      = new QLabel(tr("Replace text:"));

    _sensetive_check    = new QCheckBox(tr("Case sensetive"));
    _words_check        = new QCheckBox(tr("Find whole words"));
    // _regexp_check       = new QCheckBox(tr("Regular expression"));

    _replace_all_check  = new QCheckBox(tr("Replace all"));

    _find_button        = new QPushButton(tr("Find"));

    _replace_button     = new QPushButton(tr("Replace"));

    _layout->addWidget(_find_label);
    _layout->addWidget(_find_edit);
    // _layout->addWidget(_regexp_check);
    _layout->addWidget(_sensetive_check);
    _layout->addWidget(_words_check);
    _layout->addWidget(_replace_label);
    _layout->addWidget(_replace_edit);
    _layout->addWidget(_replace_all_check);

    _replace_all_check->setVisible(false);

    QHBoxLayout * buttons = new QHBoxLayout();
    buttons->addWidget(_find_button);
    buttons->addWidget(_replace_button);
    buttons->addStretch();

    _layout->addLayout(buttons);
    _layout->addStretch();

    setSizePolicy(QSizePolicy::Policy::Preferred, QSizePolicy::Policy::Fixed);

    resetReplaceMode(false);

    connect(_find_button, &QPushButton::clicked, this, &FindReplaceWidget::find);
    connect(_find_edit, &QLineEdit::returnPressed, this, &FindReplaceWidget::find);
    connect(_replace_button, &QPushButton::clicked, this, &FindReplaceWidget::replace);
}

FindReplaceWidget::~FindReplaceWidget()
{
}

bool FindReplaceWidget::replace_mode() const 
{ 
    return _replace_button->isVisible(); 
}

bool FindReplaceWidget::resetReplaceMode(bool turn_on)
{
    /// @todo Используется запрещённый метод getCurrentViewAdaptor. Нужно переделать!
    shell::ViewAdaptor_interface *     view           = const_cast<shell::ViewAdaptor_interface *>(_shell_access.getCurrentViewAdaptor());
    shell::ViewAdaptorFind_interface * find_interface = nullptr;

    if (view)
        find_interface = view->getFindInterface();

    _replace_label->setVisible(turn_on);
    _replace_edit->setVisible(turn_on);
    /// \todo Проблема с зацикливанием в GeneralTextViewAdaptor::replace. Нужно разбираться.
    // _replace_all_check->setVisible(turn_on);
    _replace_button->setVisible(turn_on);
    _find_button->setEnabled(find_interface != nullptr);
    // _replace_button->setEnabled(find_interface != nullptr && shell::check(view->plugin()->supports(),shell::PS_Doc_Replace));

    if (find_interface) {
        QString sample_text = find_interface->getSampleTextToFind();

        _find_edit->setText(sample_text);
        _find_edit->selectAll();

        _replace_edit->setText(sample_text);
        _replace_edit->selectAll();
    }

    return true;
}

void FindReplaceWidget::find()
{
    /// @todo Используется запрещённый метод getCurrentViewAdaptor. Нужно переделать!
    shell::ViewAdaptor_interface * view = const_cast<shell::ViewAdaptor_interface *>(_shell_access.getCurrentViewAdaptor());
    if (view != nullptr && shell::check(view->plugin()->supports(),shell::PS_Doc_Find)) {
        shell::ViewAdaptorFind_interface * find_interface = view->getFindInterface();
        if (find_interface) {
            QTextDocument::FindFlags options = QTextDocument::FindFlags();

            if (_sensetive_check->isChecked())
                options |= QTextDocument::FindCaseSensitively;

            if (_words_check->isChecked())
                options |= QTextDocument::FindWholeWords;

            find_interface->find(_find_edit->text(), options);
        }
    }
}

void FindReplaceWidget::replace()
{
    /// @todo Используется запрещённый метод getCurrentViewAdaptor. Нужно переделать!
    shell::ViewAdaptor_interface * view = const_cast<shell::ViewAdaptor_interface *>(_shell_access.getCurrentViewAdaptor());
    if (view != nullptr && shell::check(view->plugin()->supports(),shell::PS_Doc_Find)) {
        shell::ViewAdaptorFind_interface * find_interface = view->getFindInterface();
        if (find_interface) {
            QTextDocument::FindFlags options = QTextDocument::FindFlags();

            if (_sensetive_check->isChecked())
                options |= QTextDocument::FindCaseSensitively;

            if (_words_check->isChecked())
                options |= QTextDocument::FindWholeWords;

            find_interface->replace(_find_edit->text(), _replace_edit->text(), options, _replace_all_check->isChecked());
        }
    }
}

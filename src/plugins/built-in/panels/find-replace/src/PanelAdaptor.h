#ifndef PanelAdaptor_H
#define PanelAdaptor_H

#include "simodo/shell/panel/PanelAdaptor_interface.h"

#include "FindReplaceWidget.h"

namespace shell  = simodo::shell;

class PanelAdaptor : public shell::PanelAdaptor_interface
{
    FindReplaceWidget * _widget = nullptr;

public:
    PanelAdaptor(shell::Access_interface & plug_data);

    virtual QWidget * panel_widget() override { return _widget; }
    virtual QString title() const override;
    virtual void reset() override {}
    virtual void changedCurrentDocument(const QString & path) override;
    virtual bool activateService(shell::service_t service) override;
    virtual bool acceptData(const QJsonObject & ) override { return false; }
    virtual shell::AdaptorModeling_interface * getModelingInterface() override { return nullptr; }
};

#endif // PanelAdaptor_H

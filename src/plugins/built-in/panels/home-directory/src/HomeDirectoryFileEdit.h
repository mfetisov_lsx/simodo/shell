#pragma once
#include <QtWidgets>

class FileEdit : public QLineEdit
{
public:
    virtual void focusOutEvent(QFocusEvent *) override
    {
        setVisible(false);
        selectAll();
    }
};
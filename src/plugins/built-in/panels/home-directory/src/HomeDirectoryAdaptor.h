#ifndef HomeDirectoryAdaptor_H
#define HomeDirectoryAdaptor_H

#include "simodo/shell/panel/PanelAdaptor_interface.h"
#include "HomeDirectoryWidget.h"

namespace shell = simodo::shell;

class HomeDirectoryAdaptor : public shell::PanelAdaptor_interface
{
    HomeDirectoryWidget * _widget;

    shell::Access_interface & _shell;

public:
    HomeDirectoryAdaptor(shell::Access_interface & plug_data);

    virtual QWidget * panel_widget() override { return _widget; }
    virtual QString title() const override;
    virtual void reset() override;
    virtual void changedCurrentDocument(const QString & path) override;
    virtual bool activateService(shell::service_t ) override { return true; }
    virtual bool acceptData(const QJsonObject & ) override { return false; }
    virtual shell::AdaptorModeling_interface * getModelingInterface() override { return nullptr; }
};

#endif // HomeDirectoryAdaptor_H

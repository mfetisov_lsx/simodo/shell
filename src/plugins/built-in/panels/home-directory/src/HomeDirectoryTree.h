#ifndef HomeDirectoryTree_H
#define HomeDirectoryTree_H

#include "simodo/shell/access/Access_interface.h"

#include "HomeDirectorySortProxy.h"

#include <QTreeView>
#include <QFileInfo>
#include <QSortFilterProxyModel>

QT_BEGIN_NAMESPACE
class QFileSystemModel;
class QToolBar;
QT_END_NAMESPACE

namespace shell = simodo::shell;

class HomeDirectoryWidget;

class HomeDirectoryTree : public QTreeView
{
    Q_OBJECT

    QFileSystemModel &          _fs_model;
    
    HomeDirectorySortProxy *    _sort_proxy;

public:
    HomeDirectoryTree(QFileSystemModel & filesystem_model, const QStringList & hide_filenames);
    virtual ~HomeDirectoryTree() override;

    void        resetWorkDirectory();
    bool        setSelectorPosition(const QString & path);
    // void        setSearchNameToSortProxy(const QString & search_name);

    QString     addFileToDirectory(const QString & file_name);
    bool        addDirectoryToDirectory(const QString & directory_name);
    QString     addFileToHomeDirectory(const QString & file_name);
    bool        addDirectoryToHomeDirectory(const QString & directory_name);

    bool        isFileSelected();
    QString     selectedPath();

    void        editCurrent();
    void        expandCurrent();

public slots:
    // void        onFileRenamed(const QString &path, const QString &oldName, const QString &newName);
    void        onDuplicateFile();
    void        onDelete();

protected:
    QFileInfo   getFileInfo(const QModelIndex & index) const;
    QIcon       getFileIcon(const QString & file_path) const;

    bool        isValidName(const QString & name);

    virtual void mouseDoubleClickEvent(QMouseEvent *e) override;
    virtual bool event(QEvent *event) override;
};

#endif // HomeDirectoryTree_H

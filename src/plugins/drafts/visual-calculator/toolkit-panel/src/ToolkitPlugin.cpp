#include "ToolkitPlugin.h"
#include "PanelAdaptor.h"

ToolkitPlugin::ToolkitPlugin()
{
}

shell::PanelAdaptor_interface * ToolkitPlugin::createPanelAdaptor(shell::Access_interface & shell_access)
{
    return new PanelAdaptor(shell_access);
}

shell::service_t ToolkitPlugin::supports() const
{
    return shell::PS_Pan_Singleton
            ;
}

QSet<QString> ToolkitPlugin::workWithDocuments() const
{
    return {"visual-calculator"};
}

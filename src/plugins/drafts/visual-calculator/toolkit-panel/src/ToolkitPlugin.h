#ifndef ToolkitPlugin_H
#define ToolkitPlugin_H

#include "simodo/shell/panel/Panel_plugin.h"

#include <QSet>

namespace shell  = simodo::shell;

class ToolkitPlugin: public QObject, public shell::Panel_plugin
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "BMSTU.SIMODO.shell.plugin.Panel" FILE "ToolkitPlugin.json")
    Q_INTERFACES(simodo::shell::Panel_plugin)

public:
    ToolkitPlugin();

    virtual simodo::version_t version() const override { return shell::version(); }
    virtual shell::PanelAdaptor_interface * createPanelAdaptor(shell::Access_interface & plug_data) override;

    virtual QString id() const override { return "visual-calculator-toolkit"; }
    virtual QString designation() const override { return "Toolkit"; }
    virtual Qt::DockWidgetAreas allowed_areas() const override { return Qt::AllDockWidgetAreas; }
    virtual Qt::DockWidgetArea attach_to() const override { return Qt::LeftDockWidgetArea; }

    virtual shell::service_t supports() const override;

    virtual QSet<QString> workWithDocuments() const override;
};

#endif // ToolkitPlugin_H

#include "VisualDocumentPlugin.h"
#include "VisualDocumentViewAdaptor.h"

#include "widgets/AdditionModel.hpp"
#include "widgets/DivisionModel.hpp"
#include "widgets/MultiplicationModel.hpp"
#include "widgets/NumberDisplayDataModel.hpp"
#include "widgets/NumberSourceDataModel.hpp"
#include "widgets/SubtractionModel.hpp"

namespace {

static std::shared_ptr<QtNodes::NodeDelegateModelRegistry> registerDataModels()
{
    auto ret = std::make_shared<QtNodes::NodeDelegateModelRegistry>();
    ret->registerModel<NumberSourceDataModel>("Sources");

    ret->registerModel<NumberDisplayDataModel>("Displays");

    ret->registerModel<AdditionModel>("Operators");

    ret->registerModel<SubtractionModel>("Operators");

    ret->registerModel<MultiplicationModel>("Operators");

    ret->registerModel<DivisionModel>("Operators");

    return ret;
}

static void setStyle()
{
    QtNodes::ConnectionStyle::setConnectionStyle(
        R"(
  {
    "ConnectionStyle": {
      "ConstructionColor": "gray",
      "NormalColor": "black",
      "SelectedColor": "gray",
      "SelectedHaloColor": "deepskyblue",
      "HoveredColor": "deepskyblue",

      "LineWidth": 3.0,
      "ConstructionLineWidth": 2.0,
      "PointDiameter": 10.0,

      "UseDataDefinedColors": true
    }
  }
  )");
}

}

VisualDocumentPlugin::VisualDocumentPlugin()
{
    setStyle();

    _registry = registerDataModels();

    _extensions << "flow";
}

QString VisualDocumentPlugin::view_name() const
{
    return tr("Visual calculator document");
}

QString VisualDocumentPlugin::view_short_name() const
{
    return "visual calculator";
}

QString VisualDocumentPlugin::id() const
{
    return "visual-calculator";
}

QSet<QString> VisualDocumentPlugin::alternate_view_mnemonics(const QString & ) const
{
    return {{"general-text"}};
}

std::shared_ptr<shell::DocumentAdaptor_interface> VisualDocumentPlugin::createDocumentAdaptor(shell::Access_interface & shell_access)
{
    return std::make_shared<VisualDocumentAdaptor>(shell_access, _registry);
}

shell::ViewAdaptor_interface * VisualDocumentPlugin::createViewAdaptor(shell::Access_interface & shell_access,
                                                        std::shared_ptr<shell::DocumentAdaptor_interface> document_adaptor,
                                                        const QString & )
{
    return new VisualDocumentViewAdaptor(shell_access, this, document_adaptor);
}

shell::service_t VisualDocumentPlugin::supports() const
{
    return  shell::PS_None
            | shell::PS_Doc_New
            | shell::PS_Doc_Open
            | shell::PS_Doc_Save
            | shell::PS_Doc_Print
            | shell::PS_Doc_ExportToPdf
//            | shell::PS_Doc_Export
            | shell::PS_Doc_UndoRedo
//            | shell::PS_Doc_Copy
//            | shell::PS_Doc_Paste
//            | shell::PS_Doc_Zoom
            // | shell::PS_Doc_Find
            // | shell::PS_Doc_Replace
            | shell::PS_Doc_Loadable
            | shell::PS_Doc_Splittable
            ;
}

QString VisualDocumentPlugin::icon_theme() const
{
    return "";
}

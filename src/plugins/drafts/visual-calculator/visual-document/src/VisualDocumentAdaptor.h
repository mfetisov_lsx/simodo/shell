#ifndef DocumentAdaptor_H
#define DocumentAdaptor_H

#include "simodo/shell/access/Access_interface.h"
#include "simodo/shell/document/DocumentAdaptor_interface.h"

#include "QtNodes/DataFlowGraphModel"

namespace shell = simodo::shell;

class VisualDocumentAdaptor : public shell::DocumentAdaptor_interface
{
    shell::Access_interface &   _shell_access;
    QtNodes::DataFlowGraphModel _model;
    bool                        _is_modified {false};

    bool          _save_flag = false;

public:
    VisualDocumentAdaptor(shell::Access_interface &  shell_access,
                          std::shared_ptr<QtNodes::NodeDelegateModelRegistry> registry);

    virtual QString loadFile(const QString & fileName) override;
    virtual QString saveFile(const QString &fileName) override;
    virtual bool wasSaved() override;
    virtual QString text() override;
    virtual bool setText(const QString &text) override;
    virtual bool isModified() override;
    virtual void setModified(bool modified) override;

    QtNodes::DataFlowGraphModel & model() { return _model; }
};

#endif // DocumentAdaptor_H

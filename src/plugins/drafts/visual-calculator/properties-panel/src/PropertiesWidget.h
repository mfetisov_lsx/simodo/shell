#ifndef PropertiesWidget_H
#define PropertiesWidget_H

#include "simodo/shell/panel/PanelAdaptor_interface.h"
#include "simodo/shell/access/Access_interface.h"

#include <QTreeWidget>
#include <QTreeWidgetItem>

namespace shell  = simodo::shell;

class PropertiesWidget : public QTreeWidget
{
    Q_OBJECT

    shell::PanelAdaptor_interface * _adaptor;
    shell::Access_interface &       _shell_access;

public:
    PropertiesWidget(shell::PanelAdaptor_interface *adaptor, shell::Access_interface & plug_data);
    virtual ~PropertiesWidget() override;

    bool acceptData(const QJsonObject & data);

public slots:

protected:

};

#endif // PropertiesWidget_H

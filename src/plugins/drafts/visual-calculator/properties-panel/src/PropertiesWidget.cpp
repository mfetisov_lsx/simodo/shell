#include <QtWidgets>

#include "PropertiesWidget.h"
#include "PanelAdaptor.h"

PropertiesWidget::PropertiesWidget(shell::PanelAdaptor_interface *adaptor, shell::Access_interface &shell_access)
    : _adaptor(adaptor)
    , _shell_access(shell_access)
{
    setSortingEnabled(false);
    setAnimated(true);
    setUniformRowHeights(true);
    setRootIsDecorated(false);
    setExpandsOnDoubleClick(false);
    setItemsExpandable(false);
    setHeaderHidden(false);

    QTreeWidgetItem *header = new QTreeWidgetItem();
    header->setText(0, tr("Role"));
    header->setToolTip(0,tr("Role of the property"));
    header->setTextAlignment(0, Qt::AlignCenter);
    header->setText(1, tr("Type"));
    header->setToolTip(1,tr("Type of the property"));
    header->setTextAlignment(1, Qt::AlignCenter);
    header->setText(2, tr("Name"));
    header->setToolTip(2,tr("Name of the property"));
    header->setTextAlignment(2, Qt::AlignCenter);
    header->setText(3, tr("Value"));
    header->setToolTip(3,tr("Value of the property"));
    header->setTextAlignment(3, Qt::AlignCenter);

    setHeaderItem(header);
}

PropertiesWidget::~PropertiesWidget()
{
}

bool PropertiesWidget::acceptData(const QJsonObject &data)
{
    clear();

    for(auto it = data.begin(); it != data.end(); ++it) {
        QTreeWidgetItem *item = new QTreeWidgetItem();

        item->setText(0, "");
        item->setText(1, "");
        item->setTextAlignment(1, Qt::AlignCenter);
        item->setText(2, it.key());
        item->setTextAlignment(2, Qt::AlignRight);
        item->setText(3, QJsonDocument({it.value()}).toJson(QJsonDocument::Compact).toStdString().c_str());
        item->setTextAlignment(3, Qt::AlignLeft);

        addTopLevelItem(item);
    }

    resizeColumnToContents(0);
    resizeColumnToContents(1);
    resizeColumnToContents(2);
    resizeColumnToContents(3);

    update();

    return true;
}


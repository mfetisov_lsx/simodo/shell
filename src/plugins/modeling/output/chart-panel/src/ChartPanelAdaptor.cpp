#include "ChartPanelAdaptor.h"

namespace
{

void showHelp();

}

ChartPanelAdaptor::ChartPanelAdaptor(shell::Access_interface &shell)
{
    auto tab_widget = new QTabWidget;
    tab_widget->setTabBarAutoHide(true);
    tab_widget->setTabsClosable(true);
    connect(
        tab_widget
        , &QTabWidget::tabCloseRequested
        , this
        , [&shell, tab_widget](int index)
        {
            if (index < 0)
            {
                return;
            }

            QMessageBox mb;
            mb.setIcon(QMessageBox::Question);
            mb.setText(
                QObject::tr("Close chart \"") + tab_widget->tabText(index) + "\"?"
            );
            mb.setInformativeText(
                QObject::tr("Chart data will be lost after closing.")
            );
            mb.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
            mb.setDefaultButton(QMessageBox::No);
            int ret = mb.exec();
            if (ret != QMessageBox::Yes)
            {
                return;
            }

            auto tab = tab_widget->widget(index);
            tab_widget->removeTab(index);
            tab->deleteLater();
        }
        , Qt::QueuedConnection
    );

    auto controller_factory = [&shell, tab_widget]
    (QString name) -> ChartController *
    {
        auto chart = new Chart;

        auto view = new ChartView(chart);

        auto controller = new ChartController(chart);

        auto free_view_check = new QCheckBox(QObject::tr("Rubber band"));
        auto smooth_lines_check = new QCheckBox(QObject::tr("Smooth lines"));
        auto help_button = new QPushButton(QObject::tr("Help"));
        const QIcon aboutIcon = QIcon::fromTheme("help-about", QIcon(":/images/help-about"));
        help_button->setIcon(aboutIcon);

        QObject::connect(
            free_view_check, &QCheckBox::clicked
            , view, &ChartView::enableRubberBand
            , Qt::QueuedConnection
        );
        QObject::connect(
            smooth_lines_check, &QCheckBox::clicked
            , view, &ChartView::setSmoothLines
            , Qt::QueuedConnection
        );
        QObject::connect(
            view, &ChartView::smoothLinesChanged
            , smooth_lines_check, &QCheckBox::setChecked
            , Qt::QueuedConnection
        );
        QObject::connect(help_button, &QPushButton::clicked, &showHelp);

        auto check_layout = new QHBoxLayout;
        check_layout->addWidget(free_view_check);
        check_layout->addWidget(smooth_lines_check);
        check_layout->addStretch();
        check_layout->addWidget(help_button);

        auto layout = new QVBoxLayout;
        layout->addLayout(check_layout);
        layout->addWidget(view);

        auto widget = new QWidget;
        widget->setLayout(layout);

        tab_widget->addTab(widget, name);

        return controller;
    };
    auto dispatcher = new ChartDispatcher(controller_factory, tab_widget);
    connect(
        dispatcher
        , &ChartDispatcher::initRequested
        , this
        , [this, &shell](QString title)
        {
            _title = title;

            shell.setPanelTitle("Chart", title);
            shell.risePanel("Chart", title);
        }
        , Qt::QueuedConnection
    );

    _widget = tab_widget;
    _modeling_interface = dispatcher;
}

QWidget *ChartPanelAdaptor::panel_widget()
{
    return _widget;
}

QString ChartPanelAdaptor::title() const
{
    return _title;
}

void ChartPanelAdaptor::reset()
{}

void ChartPanelAdaptor::changedCurrentDocument(const QString &)
{}

bool ChartPanelAdaptor::activateService(shell::service_t)
{
    return false;
}

bool ChartPanelAdaptor::acceptData(const QJsonObject &)
{
    return false;
}

shell::AdaptorModeling_interface *ChartPanelAdaptor::getModelingInterface()
{
    return _modeling_interface;
}

namespace
{

void showHelp()
{
    QMessageBox mb;
    mb.setIcon(QMessageBox::Question);
    mb.setText(
        QObject::tr("Chart commands:")
    );
    mb.setInformativeText(
        QObject::tr(
            "Left mouse button: zoom rectangle/reset;\n"
            "Right mouse button: zoom in/out;\n"
            "Wheel: scroll up-down/right-left, zoom in/out;\n"
            "Touchpad is used as wheel."
        )
    );
    mb.setDetailedText(
        QObject::tr(
            "Buttons:\n"
            "* LMB - left mouse button;\n"
            "* RMB - right mouse button.\n"
            "\n"
            "Commands:\n"
            "* LMB: select rectangle to zoom;\n"
            "* LMB+ALT: reset zoom;\n"
            "* RMB: zoom out (factor 0.5);\n"
            "* RMB+ALT: zoom in (factor 2);\n"
            "* Wheel+CTRL: scroll up-down;\n"
            "* Wheel+SHIFT: scroll right-left;\n"
            "* Wheel+CTRL+SHIFT: zoom in/out."
        )
    );
    mb.setStandardButtons(QMessageBox::Ok);
    mb.setDefaultButton(QMessageBox::Ok);
    mb.exec();
}

}

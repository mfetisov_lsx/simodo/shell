#ifndef ChartPanelAdaptor_H
#define ChartPanelAdaptor_H

#include "domain/ChartView.h"
#include "ChartDispatcher.h"

#include "simodo/shell/panel/PanelAdaptor_interface.h"
#include "simodo/shell/access/Access_interface.h"

#include <QtCore>

class ChartPanelAdaptor : public QObject
                        , public simodo::shell::PanelAdaptor_interface
{
    Q_OBJECT

    QString _title;

public:
    ChartPanelAdaptor(simodo::shell::Access_interface &shell);

public:
    // shell::PanelAdaptor_interface
    QWidget *panel_widget() override;
    QString title() const override;
    void reset() override;
    void changedCurrentDocument(const QString &) override;
    bool activateService(simodo::shell::service_t) override;
    bool acceptData(const QJsonObject &) override;
    simodo::shell::AdaptorModeling_interface *getModelingInterface() override;

private:
    QWidget *_widget;
    simodo::shell::AdaptorModeling_interface *_modeling_interface;
};

#endif // ChartPanelAdaptor_H

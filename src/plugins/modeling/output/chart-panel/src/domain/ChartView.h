#ifndef CHARTVIEW_H
#define CHARTVIEW_H

#include "Chart.h"

class ChartView : public QtCharts::QChartView
{
    Q_OBJECT

public:
    ChartView(Chart *chart);
    ~ChartView();

public slots:
    void enableRubberBand(bool enabled);
    void setSmoothLines(bool is_set);

    void useOpenGLChangedHandler(bool used);

signals:
    void smoothLinesChanged(bool is_smooth);

protected:
    void wheelEvent(QWheelEvent *event) override;
    void mousePressEvent(QMouseEvent *event) override;
    void mouseReleaseEvent(QMouseEvent *event) override;
    void timerEvent(QTimerEvent *event) override;

private:
    int _show_timer;
    bool _is_rubber_band_enabled;

private:
    void _angleZoomEvent(QPoint delta);
    void _angleScrollEvent(QPoint delta);
};

#endif // CHARTVIEW_H

#ifndef ChartController_H
#define ChartController_H

#include "simodo/shell/panel/PanelAdaptor_interface.h"
#include "simodo/shell/access/AdaptorModeling_interface.h"
#include "simodo/shell/access/Access_interface.h"

#include "Chart.h"
#include "ChartView.h"

#include <QtCore>
#include <QtCharts>

#include <functional>
#include <optional>

namespace shell  = simodo::shell;

class ChartController : public QObject
                      , public shell::AdaptorModeling_interface
{
    Q_OBJECT

public:
    ChartController(Chart *chart);

public:
    // shell::AdaptorModeling_interface
    void started() override;
    void paused() override;
    void stoped() override;
    bool acceptData(const QString & func, QVector<QString> & data) override;

private:
    Chart     *_chart;

    using _Handler = std::function<bool(const QVector<QString> &)>;
    QHash<QString, _Handler> _handlers;
};

#endif // ChartController_H

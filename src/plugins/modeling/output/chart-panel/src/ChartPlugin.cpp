#include "ChartPlugin.h"
#include "ChartPanelAdaptor.h"

ChartPlugin::ChartPlugin()
{}


simodo::version_t ChartPlugin::version() const
{
    return simodo::shell::version();
}

QString ChartPlugin::id() const
{
    return "Chart";
}

simodo::shell::service_t ChartPlugin::supports() const
{
    return shell::PS_None
        // | shell::PS_Pan_Singleton
    ;
}

QSet<QString> ChartPlugin::workWithDocuments() const
{
    return {};
}


simodo::shell::PanelAdaptor_interface *ChartPlugin::createPanelAdaptor(
    simodo::shell::Access_interface & plug_data
)
{
    return new ChartPanelAdaptor(plug_data);
}

Qt::DockWidgetAreas ChartPlugin::allowed_areas() const
{
    return Qt::AllDockWidgetAreas;
}

Qt::DockWidgetArea ChartPlugin::attach_to() const
{
    return Qt::RightDockWidgetArea;
}

#ifndef Graph3DPanelAdaptor_H
#define Graph3DPanelAdaptor_H

#include "simodo/shell/panel/PanelAdaptor_interface.h"
#include "simodo/shell/access/AdaptorModeling_interface.h"
#include "simodo/shell/access/Access_interface.h"

#include "widget/Graph3D.h"

namespace shell  = simodo::shell;

class Graph3DPanelAdaptor : public QObject,
                            public shell::PanelAdaptor_interface,
                            public shell::AdaptorModeling_interface
{
    shell::Access_interface &       _shell;
    Graph3D *                       _graph3d;
    QWidget *                       _widget;

    QString                         _title;

public:
    Graph3DPanelAdaptor(shell::Access_interface & shell);
    ~Graph3DPanelAdaptor();

public:
    // shell::PanelAdaptor_interface
    virtual QWidget * panel_widget() override { return _widget; }
    virtual QString title() const override;
    virtual void reset() override {}
    virtual void changedCurrentDocument(const QString & ) override {}
    virtual bool activateService(shell::service_t ) override { return false; }
    virtual bool acceptData(const QJsonObject & ) override { return false; }
    virtual shell::AdaptorModeling_interface * getModelingInterface() override { return this; }

public:
    // shell::PanelAdaptorModeling_interface
    virtual void started() override;
    virtual void paused() override;
    virtual void stoped() override;
    virtual bool acceptData(const QString & func, QVector<QString> & data) override;

protected:
    virtual bool eventFilter(QObject *obj, QEvent *event) override;
};

#endif // ChartPanelAdaptor_H

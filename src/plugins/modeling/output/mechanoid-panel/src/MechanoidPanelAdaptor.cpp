#include "MechanoidPanelAdaptor.h"


MechanoidPanelAdaptor::MechanoidPanelAdaptor(shell::Access_interface & shell)
    : _shell(shell)
{
}

MechanoidPanelAdaptor::~MechanoidPanelAdaptor()
{
}

QWidget * MechanoidPanelAdaptor::panel_widget()
{
    return new QWidget();
}

QString MechanoidPanelAdaptor::title() const
{
    return _title;
}

void MechanoidPanelAdaptor::started() 
{
}

void MechanoidPanelAdaptor::paused() 
{
}

void MechanoidPanelAdaptor::stoped()
{
}

bool MechanoidPanelAdaptor::acceptData(const QString & func, QVector<QString> & data)
{
    if (func == "init") {
        if (data.size() == 0)
            return false;

        _title = data.last();

        _shell.setPanelTitle("Mechanoid", _title);
        _shell.risePanel("Mechanoid", _title);
        return true;
    }
    if (func == "add") {
        return true;
    }
    if (func == "parameter") {
        return true;
    }
    if (func == "event") {
        return true;
    }
    if (func == "location") {
        return true;
    }

    return false;
}


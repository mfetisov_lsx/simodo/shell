#include "CockpitBar.h"

#include <QtGui/QFontMetrics>

QFont CockpitBarUtils::fitTextToRect(const QString & text
                                    , QFont font
                                    , const QRect & rect
                                    )
{
    return fitTextToRectF(text, font, QRectF{ qreal(rect.x()), qreal(rect.y()), qreal(rect.width()), qreal(rect.height()) });
}

QFont CockpitBarUtils::fitTextToRectF(const QString & text
                                    , QFont font
                                    , const QRectF & rect
                                    )
{
    font.setPointSize(10);
    QFontMetrics metrics(font);
    
    while (metrics.horizontalAdvance(text) <= rect.width()
            && metrics.height() <= rect.height())
    {
        font.setPointSize(font.pointSize() + 1);
        metrics = QFontMetrics(font);
    }
    
    if (font.pointSize() != 10)
    {
        font.setPointSize(font.pointSize() - 1);
    }

    return font;
}

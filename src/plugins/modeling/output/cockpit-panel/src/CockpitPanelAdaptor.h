#ifndef CockpitPanelAdaptor_H
#define CockpitPanelAdaptor_H

#include "simodo/shell/panel/PanelAdaptor_interface.h"
#include "simodo/shell/access/AdaptorModeling_interface.h"
#include "simodo/shell/access/Access_interface.h"

#include "widget/CockpitView.h"

namespace shell  = simodo::shell;

class CockpitPanelAdaptor : public QObject,
                          public shell::PanelAdaptor_interface,
                          public shell::AdaptorModeling_interface
{
    Q_OBJECT

    shell::Access_interface & _shell;
    CockpitView *             _widget;

public:
    CockpitPanelAdaptor(shell::Access_interface & shell);
    ~CockpitPanelAdaptor();

public:
    // shell::PanelAdaptor_interface
    virtual QWidget * panel_widget() override { return _widget; }
    virtual QString title() const override;
    virtual void reset() override {}
    virtual void changedCurrentDocument(const QString & ) override {}
    virtual bool activateService(shell::service_t ) override { return false; }
    virtual bool acceptData(const QJsonObject & ) override { return false; }
    virtual shell::AdaptorModeling_interface * getModelingInterface() override { return this; }

public:
    // shell::AdaptorModeling_interface
    virtual void started() override;
    virtual void paused() override;
    virtual void stoped() override;
    virtual bool acceptData(const QString & func, QVector<QString> & data) override;
};

#endif // CockpitPanelAdaptor_H

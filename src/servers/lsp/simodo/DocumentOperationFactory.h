#ifndef FuzeDocumentOperationFactory_h
#define FuzeDocumentOperationFactory_h

#include "simodo/inout/token/BufferStream.h"
#include "simodo/lsp-server/ServerContext.h"

class DocumentOperationFactory : public simodo::lsp::DocumentOperationFactory_interface
{
    std::string _semantics_dir;
    std::string _modules_dir;
    std::string _grammar_dir;

public:
    DocumentOperationFactory(std::string semantics_dir, std::string modules_dir, std::string grammar_dir)
        : _semantics_dir(std::move(semantics_dir))
        , _modules_dir(std::move(modules_dir))
        , _grammar_dir(std::move(grammar_dir))
    {}

    virtual std::unique_ptr<simodo::lsp::DocumentOperation_interface> create(simodo::lsp::DocumentContext &doc, const std::string & languageId) override;

    const std::string & semantics_dir() const { return _semantics_dir; }
    const std::string & modules_dir() const { return _modules_dir; }
    const std::string & grammar_dir() const { return _grammar_dir; }

};

#endif

#include "script/ScriptDocumentOperation.h"
#include "simodo/lsp-client/CompletionItemKind.h"

using namespace simodo;

/// \file
/// \see https://microsoft.github.io/language-server-protocol/specifications/lsp/3.17/specification/#textDocument_completion

variable::Value ScriptDocumentOperation::produceCompletionResponse(const lsp::CompletionParams & /*completionParams*/) const
{
    std::vector<variable::Value> completion_items;

    variable::VariableSet_t vars;

    std::u16string last_production;
    for(const variable::Variable & var : _semantic_data.declared())
        if (vars.end() == std::ranges::find_if(vars, [var](const variable::Variable & v){ return v.name() == var.name(); }))
            vars.push_back(var);

    for(const variable::Variable & var : vars)
        completion_items.push_back(makeCompletionItem(var.name(), u"", u"", static_cast<int64_t>(lsp::CompletionItemKind::Variable)));

    // std::vector<std::u16string> keywords {
    //     u"main", u"include", u"remove",
    // };
    // for(const std::u16string & w : keywords)
    //     completion_items.push_back(makeCompletionItem(w, u"keyword", u"", int64_t(lsp::CompletionItemKind::Keyword)));

    return completion_items;
}

variable::Value ScriptDocumentOperation::makeCompletionItem(const std::u16string & name,
                                                            const std::u16string & detail,
                                                            const std::u16string & description,
                                                            int64_t kind)
{
    return variable::Object{{
        { u"label", name },
        { u"labelDetails", variable::Object{{
            { u"detail", detail },
            { u"description", description },
        }}},
        { u"kind", kind },
    }};
}
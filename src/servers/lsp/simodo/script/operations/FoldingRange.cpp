#include "script/ScriptDocumentOperation.h"

using namespace simodo;

/// \file
/// \todo Добавить возможность Folding Range
/// \see https://microsoft.github.io/language-server-protocol/specifications/lsp/3.17/specification/#textDocument_foldingRange

//variable::Value ScriptDocumentOperation::produceFoldingRangeResponse() const
//{
//    std::vector<variable::Value> folding_ranges;
//
//    for(const variable::Variable & var : _semantic_data.declared()) {
//        if (not isInCurrentFile(var.location())) continue;
//        if (not var.value().isFunction()) continue;
//
//        const auto & body_range = getFunctionBodyRange(var);
//
//        folding_ranges.push_back(variable::Object{{
//                                                       { u"startLine",      int64_t(body_range.start().line()) },
//                                                       { u"startCharacter", int64_t(body_range.start().character()) },
//                                                       { u"endLine",        int64_t(body_range.end().line()) },
//                                                       { u"endCharacter",   int64_t(body_range.end().character()) },
//                                                       { u"kind",           u"region" /* getFoldingRangeKindName(FoldingRangeKind::Region) */ }, // Полагаю, для функций тут Region?
//                                               }});
//    }
//    return folding_ranges;
//}
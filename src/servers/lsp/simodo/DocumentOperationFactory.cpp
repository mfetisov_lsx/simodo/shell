#include "DocumentOperationFactory.h"
#include "fuze/FuzeDocumentOperation.h"
#include "script/ScriptDocumentOperation.h"

using namespace simodo;

std::unique_ptr<lsp::DocumentOperation_interface> DocumentOperationFactory::create(lsp::DocumentContext & doc, 
                                                                                   const std::string & languageId)
{
    doc.server().log().debug("DocumentOperationFactory::create called for '" + languageId + "'");

    if (languageId == "fuze")
        return std::make_unique<FuzeDocumentOperation>(doc, *this, languageId);

    return std::make_unique<ScriptDocumentOperation>(doc, *this, languageId);
}
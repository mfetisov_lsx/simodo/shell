/*
MIT License

Copyright (c) 2021 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo
*/

/*! \file Утилита тестирования средств работы с JSON библиотеки SIMODO core. Проект SIMODO.
*/

#include "simodo/variable/json/Serialization.h"
#include "simodo/variable/json/parser/JsonRdp.h"
#include "simodo/inout/reporter/ConsoleReporter.h"
#include "simodo/inout/convert/functions.h"

#include <iostream>

using namespace simodo::inout;
using namespace simodo::variable;

namespace
{
    int jsonize (const std::string & file_name)
    {
        ConsoleReporter reporter;
        Value           value;
        JsonRdp         parser(reporter, file_name, value);

        bool ok = parser.parse();

        if (ok)
        {
            ok = saveJson(value, std::cout, false);

            if (!ok)
                std::cout << "Ошибка вывода в файл '" << file_name << "'" << std::endl;
        }

        return ok ? 0 : 1;
    }
}

int main(int argc, char *argv[])
{
    std::vector<std::string> arguments(argv + 1, argv + argc);

    std::string	file_name = "";
    bool	    error     = false;
    bool	    help      = false;

    for(size_t i=0; i < arguments.size(); ++i)
    {
        const std::string & arg = arguments[i];

        if (arg[0] == '-')
        {
            if (arg == "--help" || arg == "-h")
                help = true;
            else
                error = true;
        }
        else if (file_name.empty())
            file_name = arg;
        else
            error = true;
    }

    if (!help && file_name.empty())
        error = true;

    if (error)
    {
        std::cout << "Ошибка в параметрах запуска" << std::endl;
        help = true;
    }

    if (help)
        std::cout   << "Утилита тестирования средств работы с JSON. Проект SIMODO." << std::endl
                    << "Формат запуска:" << std::endl
                    << "    <имя утилиты> [<параметры>] <файл>" << std::endl
                    << "Параметры:" << std::endl
                    << "    -h | --help - отображение подсказки по запуску программы" << std::endl
                    ;

    if (error)
        return 1;

    if (file_name.empty())
        return 0;

    return jsonize(file_name);
}

/*
MIT License

Copyright (c) 2021 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo
*/

#include "simodo/setup/Setup.h"
#include "simodo/inout/convert/functions.h"

#include <iostream>

using namespace simodo;

int main(int argc, char *argv[])
{
    std::vector<std::string> arguments(argv + 1, argv + argc);

    if (arguments.empty()) {
        std::cout << "Не задано имя файла параметров" << std::endl;
        return 1;
    }

    setup::Setup    setup(arguments[0], arguments.size() > 1 ? arguments[1] : "");

    bool ok = setup.load();

    if (!ok) {
        std::cout << "Ошибка при загрузке: " << setup.error() << std::endl;
        // return 1;
    }

    std::cout << "Загружены параметры из файла '" << arguments[0] << "'" << std::endl;
    std::cout << "Короткое описание: '" << setup.setup().brief << "'" << std::endl;
    std::cout << "Описание:          '" << setup.setup().description << "'" << std::endl;
    std::cout << "Содержимое:" << std::endl;

    for(const setup::SetupStructure & ss : setup.setup().setup) {
        std::cout                       << ss.id            << std::endl
                  << "\tName:        '" << ss.name          << "'" << std::endl
                  << "\tBrief:       '" << ss.brief         << "'" << std::endl
                  << "\tDescription: '" << ss.description   << "'" << std::endl
                  << "\tValue:       '" << inout::toU8(variable::toString(ss.value)) << "'" << std::endl 
                  ;
    }

    return ok ? 0 : 1;
}

cmake_minimum_required(VERSION 3.8 FATAL_ERROR)

project(test-lsp-client)

file(GLOB_RECURSE CPPS  ./*.cpp )
file(GLOB_RECURSE HPPS  ./*.hpp ./*.h )

add_executable(${PROJECT_NAME} 
    ${CPPS} ${HPPS}
)

set_target_properties(${PROJECT_NAME} PROPERTIES
    CXX_STANDARD 20
    CXX_STANDARD_REQUIRED YES
    CXX_EXTENSIONS YES
)
target_include_directories(${PROJECT_NAME} PUBLIC ${CMAKE_SOURCE_DIR}/src/include)
target_link_libraries(${PROJECT_NAME} SIMODO-lsp-client)
set_target_properties(${PROJECT_NAME} PROPERTIES RUNTIME_OUTPUT_DIRECTORY ${CMAKE_SOURCE_DIR}/build/tests)


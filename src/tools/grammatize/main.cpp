/*
MIT License

Copyright (c) 2021 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo/loom
*/

/*! \file Синтаксический анализ
 *
 *  Утилита синтаксического анализа текста на любом языке, заданном грамматикой на языке SIMODO fuze.
 *
 *  Проект SIMODO.
*/

#include "simodo/inout/reporter/ConsoleReporter.h"
#include "simodo/inout/convert/functions.h"
#include "simodo/LibVersion.h"
#include "simodo/inout/token/InputStream.h"
#include "simodo/parser/fuze/fuze_file_extension.h"
#include "simodo/utility/grammatize.h"

#include <iostream>
#include <fstream>
#include <memory>
#include <algorithm>
#include <chrono>

#if __cplusplus >= __cpp_2017
#include <filesystem>
namespace fs = std::filesystem;
#else
#include <experimental/filesystem>
namespace fs = std::filesystem::experimental;
#endif

using namespace std;
using namespace simodo;
 
int main(int argc, char *argv[])
{
    vector<std::string> arguments(argv + 1, argv + argc);

    string  json_file_name      = "";
    string  st_dot_file_name    = "";
    string  dot_file_name       = "";
    string  grammar_file        = "";
    string  grammar_builder_method_string = "";
    bool    need_time_intervals = false;
    bool    need_state_transitions_info = false;
    bool    need_rules_info     = false;
    bool    need_st_info        = false;
    bool	error               = false;
    bool	help                = false;
    bool	version             = false;
    bool    need_silence        = false;
    bool    need_build_grammar  = false;
    bool    need_load_grammar   = false;
    bool    need_analyze_inserts= false;

    for(size_t i=0; i < arguments.size(); ++i)
    {
        const string & arg = arguments[i];

        if (arg[0] == '-')
        {
            if (arg == "--help" || arg == "-h")
                help = true;
            else if (arg == "--version" || arg == "-v")
                version = true;
            else if (arg == "--grammar-builder-method" || arg == "-z")
            {
                if (i == arguments.size()-1 || !grammar_builder_method_string.empty())
                    error = true;
                else
                    grammar_builder_method_string = arguments[++i];
            }
            else if (arg == "--json-semantics" || arg == "-j")
            {
                if (i == arguments.size()-1 || !json_file_name.empty())
                    error = true;
                else
                    json_file_name = arguments[++i];
            }
            else if (arg == "--dot-semantics" || arg == "-s")
            {
                if (i == arguments.size()-1 || !st_dot_file_name.empty())
                    error = true;
                else
                    st_dot_file_name = arguments[++i];
            }
            else if (arg == "--dot-grammar" || arg == "-d")
            {
                if (i == arguments.size()-1 || !st_dot_file_name.empty())
                    error = true;
                else
                    dot_file_name = arguments[++i];
            }
            else if (arg == "--state-transitions-info" || arg == "-i")
                need_state_transitions_info = true;
            else if (arg == "--rules-info" || arg == "-r")
                need_rules_info = true;
            else if (arg == "--insertion-info" || arg == "-o")
                need_st_info = true;
            else if (arg == "--time-intervals" || arg == "-t")
                need_time_intervals = true;
            else if (arg == "--silence" || arg == "-S")
                need_silence = true;
            else if (arg == "--dump-grammar" || arg == "-u")
                need_build_grammar = true;
            else if (arg == "--load-grammar" || arg == "-l")
                need_load_grammar = true;
            else if (arg == "--analyze-inserts" || arg == "-a")
                need_analyze_inserts = true;
            else
                error = true;
        }
        else if (grammar_file.empty())
            grammar_file = arg;
        else
            error = true;
    }

    parser::TableBuildMethod grammar_builder_method = parser::TableBuildMethod::LR1;

    if (grammar_builder_method_string == "lr" || grammar_builder_method_string == "lr1")
        grammar_builder_method = parser::TableBuildMethod::LR1;
    else if (grammar_builder_method_string == "slr")
        grammar_builder_method = parser::TableBuildMethod::SLR;
    else if (!grammar_builder_method_string.empty()) {
        cout << "Задан недопустимый метод формирования таблицы грамматики" << endl;
        error = true;
    }

    if (grammar_file.empty() && !version && !help)
        error = true;

    if (need_load_grammar && (need_build_grammar || need_time_intervals))
        error = true;

    if (need_load_grammar && (!json_file_name.empty() || !st_dot_file_name.empty())) {
        cout << "Нельзя построить JSON или граф семантического дерева из загружаемой грамматики" << endl;
        error = true;
    }

    if (error) {
        cout << "Ошибка в параметрах запуска" << endl;
        help = true;
    }

    const string logo = "Утилита анализа грамматики. Проект SIMODO.";

    if (help)
        cout	<< logo << endl
                << "Формат запуска:" << endl
                << "    simodo-grammatize [<параметры>] <файл>" << endl
                << "Параметры:" << endl
                << "    -h | --help                   - отображение подсказки по запуску программы" << endl
                << "    -v | --version                - отображение версии программы" << endl
                << "    -z | --grammar-builder-method <метод> - метод построения таблицы грамматики (slr,lr1)" << endl
                << "    -t | --time-intervals         - отображать интервалы времени разбора" << endl
                << "    -j | --json-semantics <путь>  - создать JSON-файл абстрактного дерева операционной семантики" << endl
                << "    -s | --dot-semantics <путь>   - создать DOT-файл абстрактного дерева операционной семантики" << endl
                << "    -d | --dot-grammar <путь>     - создать DOT-файл графа переходов состояний грамматики" << endl
                << "    -r | --rules-info             - вывести перечень правил грамматики" << endl
                << "    -i | --state-transitions-info - вывести информацию о переходах состояний грамматики" << endl
                << "    -o | --insertion-info         - вывести структуры семантических вставок" << endl
                << "    -S | --silence                - не выводить диагностику утилиты" << endl
                << "    -u | --dump-grammar           - построить и сохранить грамматику в дампе" << endl
                << "    -l | --load-grammar           - загрузить грамматику из дампа" << endl
                << "    -a | --analyze-inserts        - анализ вставок кода" << endl
                ;

    if (error)
        return 1;

    if (version)
        cout << logo << endl
             << "Версия: " << lib_version().major() << "." << lib_version().minor() << endl;

    if (grammar_file.empty())
        return 0;

    fs::path grammar_path = grammar_file;

    if (grammar_path.extension().empty())
        grammar_path += parser::FUZE_FILE_EXTENSION;

    std::ifstream in(grammar_path);

    if (!in) {
        cout << "Ошибка при открытии файла '" << grammar_path.string() << "'" << endl;
        return 1;
    }

    inout::InputStream      in_stream(in);
    inout::ConsoleReporter  m;
    parser::Grammar         grammar;

    return utility::grammatize(
                        grammar_file,
                        in_stream,
                        cout,
                        m,
                        json_file_name,
                        st_dot_file_name,
                        grammar_builder_method,
                        dot_file_name,
                        need_state_transitions_info,
                        need_rules_info,
                        need_st_info,
                        need_time_intervals,
                        need_silence,
                        need_build_grammar,
                        need_load_grammar,
                        need_analyze_inserts,
                        need_analyze_inserts,
                        grammar) ? 0 : 1;
}

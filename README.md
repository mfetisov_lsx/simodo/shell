﻿![SIMODO/loom title](doc/img/simodo-loom-title.jpg)

# SIMODO/loom

SIMODO/loom является разрабатываемой в настоящее время версией проекта адаптивной системы имитационного моделирования SIMODO.

**Основная задача проекта** &mdash; предоставить программные средства математического моделирования для исследования комплексных моделей, захватывающих несколько предметных областей.

УГТ4 - Технология, проверенная в лаборатории.

# Краткое описание

_Адаптивная система моделирования_ &mdash; новое понятие в имитационном математическом моделировании, когда в основе системы лежит набор предметно-ориентированных языков (ПОЯ). Каждый из таких языков отвечает только за свою предметную область, которую он описывает наиболее полно и в то же время просто.

_Адаптивная система моделирования_ подразумевает предоставление технологии построения ПОЯ. Кроме того предполагается, что система должна иметь открытую программную архитектуру, позволяющую добавлять в неё без перекомпиляции семантические модули новых языков, плагины визуализации и редактирования, модули для использования из языков и другие расширения.

Состав и возможности проекта SIMODO/loom:
* Технология разработки ПОЯ.
* Несколько готовых языков, доступных для модификации: базовый императивный язык для описания алгоритмов и декларативный язык описания системы обыкновенных дифференциальных уравнений.
* Система позволяет выполнять длительный цикл моделирования (часы, сутки, недели...) с возможностью "на лету" не только изменять параметры модели, но и части модели.
* Выполнение моделирования в многопоточной и распределённой среде, что включено в конструкции базового языка.
* Интегрированная среда разработки SIMODO shell с набором плагинов для редактирования текстов, просмотра результатов в процессе моделирования и других функций.
* Интеграция языков в среду разработки SIMODO shell с использованием технологии [Language Server Protocol](https://microsoft.github.io/language-server-protocol/specifications/lsp/3.17/specification/#headerPart).
* Возможность добавления или замены компонентов открытой архитектуры: плагинов, модулей, языков и др.
* В ближайшее время система будет иметь средства _визуального моделирования_ &mdash; описание модели с использованием блоков.

# Простой пример 

![SIMODO shell - Lorenz system](doc/img/simodo-lorenz-dark.png)

На рисунке видно, что модель аттрактора Лоренца формируется в отдельном файле на специализированном языке дифференциальных уравнений. Т.о. модель отделена от сценария моделирования. 

Сценарий моделирования, записанный на базовом императивном языке, позволяет работать с моделью, как с типом, создавая произвольное количество объектов этого типа и добавлять эти объекты на сцену моделирования. Такой подход позволяет формировать состав сцены не только перед началом моделирования, но и в процессе.

# Статьи

Более подробное описание концепции SIMODO/loom можно почитать в следующих статьях:

* [Унификация работы с ПОЯ и открытая программная архитектура в АСМ SIMODO](doc/articles/unify.pdf)
* [Адаптивная система моделирования как единая платформа отраслевых САПР](doc/articles/ams.pdf)

# Сборка проекта

Проект рекомендуется собирать в GNU/Linux. Состав необходимых библиотек и способ их установки можно посмотреть в файле сценария непрерывной интеграции `.gitlab-ci.yml`. Используются настройки для Ubuntu и Alt Linux.

После клонирования или копирования файлов для сборки проекта нужно выполнить следующие команды в каталоге проекта:

```sh
./configure
go/make
```

Запуск проверочных тестов:

```sh
test/lib
```

Запуск интегрированной среды разработки:

```sh
bin/simodo-shell
```

# Дистрибутивы проекта

Дистрибутивы проекта формируются автоматически при слиянии в ветку `prod` и выкладываются на сайт проекта [simodo.ru](http://simodo.ru).

Формируются дистрибутивы для платформ Linux и Windows.
﻿// Write your modules here!
module rk(
  input logic clk,
  input logic rst,
  input logic cs,
  input logic inc,
  input logic [7:0] d_in,
  output logic data_valid,
  output logic [7:0] d_out
);
  always_ff @(posedge clk, posedge rst)
  begin
    if (rst)
      begin
      	data_valid <= 1;
        d_out <= 'b00000000;
      end
    else if (clk)
      begin
      	data_valid <= cs;
        if (!cs)
          if (inc)
            d_out <= d_in + 1;
          else
            d_out <= d_in - 1;
      end
  end
endmodule

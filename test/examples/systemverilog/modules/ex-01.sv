﻿module ex(
  input logic in_value,  
  output logic out_value
);
  always_ff @ (posedge in_value)
  begin
    out_value[0] <= 1 - in_value[0];
  end  
endmodule
